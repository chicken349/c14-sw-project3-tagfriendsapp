import {pointLocation} from '../../helpers/types';

export type ScheduleData = {
  id: number;
  inviter_id: number;
  invitee_id: number;
  name: string;
  vicinity: string;
  location: pointLocation;
  is_accepted: boolean;
  place_id: string;
  rating: number;
  scheduled_at: string;
  description: string | null;
  friend: string;
  profilePic: string;
};

export type SocketScheduleData = {
  id: number;
  inviter_id: number;
  invitee_id: number;
  is_accepted: boolean;
  roomId: string;
};

export type ScheduleState =
  | {
      status: 'ready';
      msg?: string;
      scheduleData?: ScheduleData[];
    }
  | {
      status: 'error';
      msg?: string;
      scheduleData?: ScheduleData[];
    }
  | {
      status: 'wentOutFromSchedule';
      msg?: string;
      scheduleData: undefined;
    }
  | {
      status: 'loading';
      msg?: string;
      scheduleData?: ScheduleData[];
    };

// new getSchedule [
//   {
//     id: 3,
//     inviter_id: 8,
//     invitee_id: 3,
//     name: '天外天',
//     place_id: 'ChIJD8VZ9eUABDQRhDmubm8Q9Hw',
//     location: { x: 114.1783443201073, y: 22.29940862010728 },
//     vicinity: '尖沙咀科學館道17號',
//     rating: 4.4,
//     scheduled_at: 2021-07-03T05:05:49.649Z,
//     description: null,
//     friend: 'Adrian Todd',
//     profilePic: 'photo/asian_female-1624683946752.jpeg'
//   }
// ]

export const initialState: ScheduleState = {
  status: 'loading',
};
