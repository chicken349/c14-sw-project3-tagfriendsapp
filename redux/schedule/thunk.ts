// import {Alert} from 'react-native';
import {api} from '../../helpers/api';
import {ILocation} from '../../helpers/types';
import {IRootState, IRootThunkDispatch} from '../store';
import {failed, loadedSchedule, loading} from './action';

export function loadScheduleThunk() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      let token = getState().auth.token;
      let res = await api.get('/schedule', token);
      if (res.status !== 200) {
        throw await res.json();
      }
      let result = await res.json();
      console.log('schedule record: ', result.data);
      // dispatch(loaded());
      dispatch(loadedSchedule(result.data));
    } catch (e) {
      dispatch(failed('get schedule failed' + e.msg + e.message));
      // dispatch(loaded());
      // Alert.alert('網絡連接失敗', '請重新檢查網絡', [
      //   {text: '確認', style: 'default'},
      // ]);
      return;
    }
  };
}

export function recordSchedule(input: {
  room_id: string;
  place_id: string;
  location: ILocation;
  vicinity: string; //address
  rating: number;
  name: string;
  description: string;
  schedule_date: string;
  searchKey: string;
}) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    try {
      let token = getState().auth.token;
      console.log('input: ', input);
      let res = await api.post('/recordSchedule', input, token);
      if (res.status !== 200) {
        throw await res.json();
      }
      let result = await res.json();
      console.log('saved schedule id: ' + result.data);
      // dispatch(loaded());
    } catch (e) {
      dispatch(failed('get schedule failed' + e.msg + e.message));
      // dispatch(loaded());
      // Alert.alert('網絡連接失敗', '請重新檢查網絡', [
      //   {text: '確認', style: 'default'},
      // ]);
      return;
    }
  };
}

export function replySchedule(
  ans: boolean | null,
  scheduleID: number,
  roomId: string,
  place_name: string,
) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    try {
      let token = getState().auth.token;
      let res = await api.post(
        '/replySchedule',
        {ans, scheduleID, roomId, place_name},
        token,
      );
      if (res.status !== 201) {
        throw await res.json();
      }
      // dispatch(loaded());
    } catch (e) {
      dispatch(failed('get schedule failed' + e.msg + e.message));
      // dispatch(loaded());
      // Alert.alert('網絡連接失敗', '請重新檢查網絡', [
      //   {text: '確認', style: 'default'},
      // ]);
      return;
    }
  };
}
