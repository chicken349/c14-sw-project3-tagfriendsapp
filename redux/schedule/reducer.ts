import {ScheduleAction} from './action';
import {initialState, ScheduleState} from './state';

export const ScheduleReducer = (
  state: ScheduleState = initialState,
  action: ScheduleAction,
): ScheduleState => {
  switch (action.type) {
    case '@@Schedule/failed':
      return {
        ...state,
        status: 'error',
      };

    // case '@@Schedule/loaded':
    //   return {
    //     ...state,
    //     status: 'loaded',
    //   };

    case '@@Schedule/loading':
      return {
        ...state,
        status: 'loading',
      };

    case '@@Schedule/loadedSchedule':
      return {
        ...state,
        status: 'ready',
        scheduleData: action.scheduleData,
      };

    case '@@Schedule/wentOutFromSchedule':
      return {
        ...state,
        status: 'loading',
        scheduleData: undefined,
      };

    default:
      return state;
  }
};
