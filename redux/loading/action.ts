export function loaded() {
  return {
    type: '@@Loading/loaded' as const,
  };
}

export function loading() {
  return {
    type: '@@Loading/loading' as const,
  };
}

export type LoadingAction =
  | ReturnType<typeof loaded>
  | ReturnType<typeof loading>;
