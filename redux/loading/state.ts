export type LoadingState =
  | {
      status: 'loading';
    }
  | {
      status: 'loaded';
    };

export const initialState: LoadingState = {
  status: 'loading',
};
