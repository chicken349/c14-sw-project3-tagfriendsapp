import {CoinRecord} from '../../helpers/types';

export type CoinRecordState =
  | {
      status: 'ready';
      msg?: string;
      coinRecord?: CoinRecord[];
    }
  | {
      status: 'error';
      msg: string;
      coinRecord?: CoinRecord[];
    }
  | {
      status: 'loading';
      msg?: string;
      coinRecord?: CoinRecord[];
    }
  | {
      status: 'loaded';
      msg?: string;
      coinRecord?: CoinRecord[];
    };

export const initialState: CoinRecordState = {
  status: 'loading',
};
