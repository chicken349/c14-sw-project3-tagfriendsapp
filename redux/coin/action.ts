import {CoinRecord} from '../../helpers/types';

export function loadCoinRecord(coinRecord: CoinRecord[]) {
  return {
    type: '@@CoinRecord/loadCoinRecord' as const,
    coinRecord,
  };
}

export function failed(msg: string) {
  console.log('failed to load coin records', msg);
  return {
    type: '@@CoinRecord/failed' as const,
    msg,
  };
}

export function loaded() {
  console.log('loaded coin records');
  return {
    type: '@@CoinRecord/loaded' as const,
  };
}

export function loading() {
  console.log('loading coin records');
  return {
    type: '@@CoinRecord/loading' as const,
  };
}

export type CoinRecordAction =
  | ReturnType<typeof loadCoinRecord>
  | ReturnType<typeof failed>
  | ReturnType<typeof loaded>
  | ReturnType<typeof loading>;
