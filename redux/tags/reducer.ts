import {TagsState, initialState} from './state';
import {TagsAction} from './action';

export const TagsReducer = (
  state: TagsState = initialState,
  action: TagsAction,
): TagsState => {
  switch (action.type) {
    case '@@Tags/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Tags/loading':
      return {
        ...state,
        status: 'loading',
      };
    case '@@Tags/loadedUserTags': {
      return {
        ...state,
        status: 'loadedUserTags',
        userTags: action.userTags,
      };
    }
    case '@@Tags/loadedUserWishTags': {
      return {
        ...state,
        status: 'loadedUserWishTags',
        userWishTags: action.userWishTags,
      };
    }
    case '@@Tags/loadedAllClassesAndTags': {
      return {
        ...state,
        status: 'loadedAllClassesAndTags',
        tagClassesAndTags: action.tagClassesAndTags,
      };
    }
    case '@@Tags/editedWishTags': {
      return {
        ...state,
        status: 'editedWishTags',
        msg: action.isSuccess ? 'success' : 'failed',
        userWishTags: action.userWishTags,
      };
    }
    case '@@Tags/addedWishTags': {
      return {
        ...state,
        status: 'addedWishTags',
        msg: action.isSuccess ? 'success' : 'failed',
        userWishTags: action.userWishTags,
      };
    }
    case '@@Tags/deletedWishTags': {
      return {
        ...state,
        status: 'deletedWishTags',
        msg: action.isSuccess ? 'success' : 'failed',
        userWishTags: action.userWishTags,
      };
    }
    case '@@Tags/editedUserTags': {
      return {
        ...state,
        status: 'editedUserTags',
        msg: action.isSuccess ? 'success' : 'failed',
        userTags: action.userTags,
      };
    }
    case '@@Tags/addedUserTags': {
      return {
        ...state,
        status: 'addedUserTags',
        msg: action.isSuccess ? 'success' : 'failed',
        userTags: action.userTags,
      };
    }
    case '@@Tags/deletedUserTags': {
      return {
        ...state,
        status: 'deletedUserTags',
        msg: action.isSuccess ? 'success' : 'failed',
        userTags: action.userTags,
      };
    }
    default:
      return state;
  }
};
