import {TagClass, UserTags, UserWishTags} from '../../helpers/types';

export type TagsState =
  | {
      status: 'loading';
      msg?: string;
      userTags?: UserTags;
      userWishTags?: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'error';
      msg: string;
      userTags?: UserTags;
      userWishTags?: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'loadedUserTags';
      msg?: string;
      userTags: UserTags;
      userWishTags?: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'loadedAllClassesAndTags';
      msg?: string;
      userTags?: UserTags;
      userWishTags?: UserWishTags;
      tagClassesAndTags: TagClass[];
    }
  | {
      status: 'editedWishTags';
      msg: string;
      userTags?: UserTags;
      userWishTags: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'addedWishTags';
      msg: string;
      userTags?: UserTags;
      userWishTags: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'deletedWishTags';
      msg: string;
      userTags?: UserTags;
      userWishTags: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'editedUserTags';
      msg: string;
      userTags: UserTags;
      userWishTags?: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'addedUserTags';
      msg: string;
      userTags: UserTags;
      userWishTags?: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'deletedUserTags';
      msg: string;
      userTags: UserTags;
      userWishTags?: UserWishTags;
      tagClassesAndTags?: TagClass[];
    }
  | {
      status: 'loadedUserWishTags';
      msg?: string;
      userTags?: UserTags;
      userWishTags: UserWishTags;
      tagClassesAndTags?: TagClass[];
    };

export const initialState: TagsState = {
  status: 'loading',
};
