import {ILocation, MatchResult} from '../../helpers/types';

export type MatchState =
  | {
      status: 'loading';
      msg?: string;
      matchResult?: MatchResult;
      location?: ILocation;
      userPermitted?: boolean;
      insertedGeoTime?: Date;
      likeStatus?: any[];
      isAfter15Mins?: boolean;
    }
  | {
      status: 'error';
      msg: string;
      matchResult?: MatchResult;
      location?: ILocation;
      userPermitted?: boolean;
      insertedGeoTime?: Date;
      likeStatus?: any[];
      isAfter15Mins?: boolean;
    }
  | {
      status: 'located';
      msg?: string;
      matchResult?: MatchResult;
      location: ILocation;
      userPermitted?: boolean;
      insertedGeoTime?: Date;
      likeStatus?: any[];
      isAfter15Mins?: boolean;
    }
  | {
      status: 'gotUserPermission';
      msg?: string;
      matchResult?: MatchResult;
      location?: ILocation;
      userPermitted: boolean;
      insertedGeoTime?: Date;
      likeStatus?: any[];
      isAfter15Mins?: boolean;
    }
  | {
      status: 'likedPeople';
      msg?: string;
      matchResult?: MatchResult;
      location?: ILocation;
      userPermitted?: boolean;
      insertedGeoTime?: Date;
      likeStatus: any[];
      isAfter15Mins?: boolean;
    }
  | {
      status: 'unlikedPeople';
      msg?: string;
      matchResult?: MatchResult;
      location?: ILocation;
      userPermitted?: boolean;
      insertedGeoTime?: Date;
      likeStatus: any[];
      isAfter15Mins?: boolean;
    }
  | {
      status: 'insertedGeolocation';
      msg?: string;
      matchResult?: MatchResult;
      location?: ILocation;
      userPermitted?: boolean;
      insertedGeoTime: Date;
      likeStatus?: any[];
      isAfter15Mins?: boolean;
    }
  | {
      status: 'checkedIfAfter15MinsFromLastMatch';
      msg?: string;
      matchResult?: MatchResult;
      location?: ILocation;
      userPermitted?: boolean;
      insertedGeoTime?: Date;
      likeStatus?: any[];
      isAfter15Mins: boolean;
    }
  | {
      status: 'updatedMatchResult';
      msg?: string;
      matchResult: MatchResult;
      location?: ILocation;
      userPermitted?: boolean;
      insertedGeoTime?: Date;
      likeStatus?: any[];
      isAfter15Mins?: boolean;
    }
  | {
      status: 'matched';
      msg?: string;
      matchResult: MatchResult;
      location?: ILocation;
      userPermitted?: boolean;
      insertedGeoTime?: Date;
      likeStatus?: any[];
      isAfter15Mins?: boolean;
    };

export const initialState: MatchState = {
  status: 'loading',
};
