import {IRootState, IRootThunkDispatch} from '../store';
import {api} from '../../helpers/api';
import {Alert} from 'react-native';
import {submitReport} from './action';
import {loaded, loading} from '../loading/action';

export function submitReportThunk(
  input: {
    title: string;
    content: string;
    targetID: number;
  },
  navigation: any,
) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    try {
      dispatch(loading());
      console.log('input: ', input);
      let token = getState().auth.token;
      let res = await api.post('/submitReport', input, token);
      if (res.status !== 200) {
        throw new Error(await res.json());
      }
      Alert.alert('提交成功', '我們短期內會以電郵回覆閣下，謝謝', [
        {
          text: '確認',
          onPress: () => {
            navigation.goBack();
          },
          style: 'default',
        },
      ]);
      dispatch(submitReport());
      dispatch(loaded());
    } catch (e) {
      Alert.alert('提交失敗', '請再次提交', [{text: '確認', style: 'default'}]);
      dispatch(loaded());
      return;
    }
  };
}
