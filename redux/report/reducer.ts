import {ReportAction} from './action';
import {initialState, ReportState} from './state';

export const reportReducer = (
  state: ReportState = initialState,
  action: ReportAction,
): ReportState => {
  switch (action.type) {
    case '@@Report/submit_report':
      return {
        ...state,
        status: 'reported',
      };

    case '@@Report/resume_normal':
      return {
        ...state,
        status: 'normal',
      };

    default:
      return state;
  }
};
