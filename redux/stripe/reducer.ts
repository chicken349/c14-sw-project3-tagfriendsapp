// import {Alert} from 'react-native';
import {StripeAction} from './action';
import {initialState, StripeState} from './state';

export const StripeReducer = (
  state: StripeState = initialState,
  action: StripeAction,
): StripeState => {
  switch (action.type) {
    case '@@Stripe/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Stripe/SuccessPayment':
      return {
        ...state,
        status: 'successful',
      };
    case '@@Stripe/reset':
      return {
        ...state,
        status: 'ready',
      };
    case '@@Stripe/loading':
      return {
        ...state,
        status: 'loading',
      };
    case '@@Stripe/cancel':
      return {
        ...state,
        status: 'cancel',
      };
    default:
      return state;
  }
};
