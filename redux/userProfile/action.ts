import {UserProfile, UserProfilePic} from './state';

export function loadedUserProfile(userProfile: UserProfile) {
  return {
    type: '@@Profile/loadedUserProfile' as const,
    userProfile,
  };
}

export function loadedUserProfilePic(userProfilePic: UserProfilePic) {
  return {
    type: '@@Profile/loadedUserProfilePic' as const,
    userProfilePic,
  };
}

export function editedUserProfile(
  isSuccess: boolean,
  userProfile: UserProfile,
) {
  return {
    type: '@@Profile/editedUserProfile' as const,
    isSuccess,
    userProfile,
  };
}

export function editCoins(coins: number) {
  return {
    type: '@@Profile/editCoins' as const,
    coins,
  };
}

export function editedUserProfilePic(
  isSuccess: boolean,
  userProfilePic: UserProfilePic,
) {
  return {
    type: '@@Profile/editedUserProfilePic' as const,
    isSuccess,
    userProfilePic,
  };
}

export function updateUserCoin(userProfile: UserProfile) {
  return {
    type: '@@Profile/updateUserCoin' as const,
    userProfile,
  };
}

export function loading() {
  return {
    type: '@@Profile/loading' as const,
  };
}

export function failed(msg: string) {
  console.log('Profile failed', msg);
  return {
    type: '@@Profile/failed' as const,
    msg,
  };
}

export type UserProfileAction =
  | ReturnType<typeof loadedUserProfile>
  | ReturnType<typeof loadedUserProfilePic>
  | ReturnType<typeof editedUserProfile>
  | ReturnType<typeof editedUserProfilePic>
  | ReturnType<typeof updateUserCoin>
  | ReturnType<typeof editCoins>
  | ReturnType<typeof loading>
  | ReturnType<typeof failed>;
