export type UserProfile = {
  info: {
    username: string;
    email: string;
    coins: number;
    text_description: string;
    gender: string;
    birthday: Date;
  };
  matchNum: number;
};

export type UserProfilePic = {
  userProfilePic: {
    url: string;
    description: string;
  }[];
};

export type UserEditable = {
  username: string | undefined;
  gender: {
    pic: string;
    DB: string;
  };
  birthday: Date;
  text_description: string | undefined;
};

export type UserProfileState =
  | {
      status: 'loading';
      msg?: string;
      userProfile?: UserProfile;
      userProfilePic?: UserProfilePic;
    }
  | {
      status: 'error';
      msg: string;
      userProfile?: UserProfile;
      userProfilePic?: UserProfilePic;
    }
  | {
      status: 'loadedUserProfile';
      msg?: string;
      userProfile: UserProfile;
      userProfilePic?: UserProfilePic;
    }
  | {
      status: 'loadedUserProfilePic';
      msg?: string;
      userProfile?: UserProfile;
      userProfilePic: UserProfilePic;
    }
  | {
      status: 'editedUserProfile';
      msg?: string;
      userProfile: UserProfile;
      userProfilePic?: UserProfilePic;
    }
  | {
      status: 'editedUserProfilePic';
      msg?: string;
      userProfile?: UserProfile;
      userProfilePic: UserProfilePic;
    };

export const initialState: UserProfileState = {
  status: 'loading',
};
