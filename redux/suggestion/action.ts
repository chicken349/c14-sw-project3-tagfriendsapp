import {pointLocation} from '../../helpers/types';
import {GoogleSuggestMarker, Place} from './state';

export function loadedMapSuggestion(markers: GoogleSuggestMarker[][]) {
  return {
    type: '@@Suggestion/loadedMapSuggestion' as const,
    markers,
  };
}

export function updatedCurrentRoomId(roomId: string) {
  console.log('roomID updated', roomId);
  return {
    type: '@@Suggestion/updatedCurrentRoomId' as const,
    roomId,
  };
}

export function loadedSuggestPlaces(places: {
  location: pointLocation;
  suggestion: Place[];
}) {
  console.log('action', places.suggestion);
  return {
    type: '@@Suggestion/loadedSuggestPlaces' as const,
    suggestion: places.suggestion,
    location: places.location,
  };
}

export function loading() {
  return {
    type: '@@Suggestion/loading' as const,
  };
}

export function wentOutFromMap() {
  return {
    type: '@@Suggestion/wentOutFromMap' as const,
  };
}

export function ready() {
  return {
    type: '@@Suggestion/ready' as const,
  };
}

export function failed(msg: string) {
  console.log('Suggestion failed', msg);
  return {
    type: '@@Suggestion/failed' as const,
    msg,
  };
}

export type SuggestionAction =
  | ReturnType<typeof loadedMapSuggestion>
  | ReturnType<typeof ready>
  | ReturnType<typeof wentOutFromMap>
  | ReturnType<typeof updatedCurrentRoomId>
  | ReturnType<typeof loadedSuggestPlaces>
  | ReturnType<typeof loading>
  | ReturnType<typeof failed>;
