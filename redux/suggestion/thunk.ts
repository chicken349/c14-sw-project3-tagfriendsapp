import {api, Google} from '../../helpers/api';
import {
  loading,
  failed,
  loadedMapSuggestion,
  loadedSuggestPlaces,
  ready,
  updatedCurrentRoomId,
} from './action';

import {IRootThunkDispatch, IRootState} from '../store';
import {GoogleSuggestMarker, Place} from './state';
import {pointLocation} from '../../helpers/types';

export function loadSuggestPlaces(room_id: string) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const res = await api.post('/suggestion', {room_id}, userToken);
      if (res.status !== 201) {
        throw await res.json();
      }
      const result: {
        data: {location: pointLocation; suggestion: Place[]};
      } = await res.json();
      console.log('loadedSuggestPlaces', result.data);
      // console.log('loadUserWishTags', result.data.wishTags);
      dispatch(loadedSuggestPlaces(result.data));
      dispatch(loadMapSuggestion(result.data.suggestion, result.data.location));
    } catch (error) {
      // Alert.alert('讀取你的資料時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(
        failed(
          'failed to get loadedSuggestPlaces: ' + error.msg + error.message,
        ),
      );
      return;
    }
  };
}

export function loadMapSuggestion(places: Place[], location: pointLocation) {
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(loading());
    try {
      const suggestion = [];
      for (let i = 0; i < places.length; i++) {
        const queryResult = await api.get(
          `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${places[i].place}&location=${location.y},${location.x}&rankby=distance&language=zh-HK&key=${Google}`,
        );
        if (queryResult.status !== 200) {
          throw new Error('google map result error');
        }
        const response = await queryResult.json();
        // console.log('ggmap', response);
        if (response.results.length === 0) {
          continue;
        }
        const mappedResponse: GoogleSuggestMarker[] = response.results
          .filter(
            (item: any) =>
              item.business_status === 'OPERATIONAL' || !item.business_status,
          )
          .map((res: any) => ({
            coordinate: {
              latitude: res.geometry.location.lat,
              longitude: res.geometry.location.lng,
            },
            name: res.name,
            rating: res.rating,
            place_id: res.place_id,
            detail: places[i].category,
            address: res.formatted_address,
            photo: res.photos?.[0]?.photo_reference,
          }));
        suggestion.push(mappedResponse);
      }
      dispatch(loadedMapSuggestion(suggestion));
      dispatch(ready());
    } catch (error) {
      dispatch(
        failed('failed to get loadMapSuggestion: ' + error.msg + error.message),
      );
      return;
    }
  };
}

export function updateCurrentRoomId(room_id: string) {
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(updatedCurrentRoomId(room_id));
    dispatch(ready());
  };
}
