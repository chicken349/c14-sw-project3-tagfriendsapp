import {api} from '../../helpers/api';
import {
  failed,
  logout as logoutAction,
  loadToken,
  updatedToken,
  confirmedToBeRegStatThree,
  loading,
} from './action';
import {IRootThunkDispatch, IRootState} from '../../redux/store';
import {Alert} from 'react-native';
// @ts-ignore
import RNRestart from 'react-native-restart';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginInput, RegistrationInput} from '../../helpers/types';
import {JWTPayload} from '../../helpers/types';
import jwt_decode from 'jwt-decode';
// import {resetStripe} from '../stripe/action';
// import {googleResult} from '../../components/googleMap/mapData';
// import {loaded, loading} from '../loading/action';

export function logout() {
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(loading());
    try {
      console.log('logout');
      await AsyncStorage.removeItem('token');
      // await AsyncStorage.clear();
      dispatch(logoutAction());
      // dispatch(loaded());
    } catch (e) {
      dispatch(failed('fail logout' + e.msg + e.message));
      // dispatch(loaded());
    }

    // let res = await api.post('/logout').catch(err => {
    //   console.error('failed to logout', err);
    //   return {status: 500, statusText: 'Network Failure'};
    // });
    // if (res.status >= 200 && res.status < 300) {
    //   dispatch(logoutAction());
    // } else {
    //   dispatch(failed('failed to logout: ' + res.statusText));
    // }
  };
}

export function login(input: LoginInput) {
  return async (dispatch: IRootThunkDispatch) => {
    let token: string;
    try {
      let res = await api.post('/login', input);
      let result = await res.json();
      if (res.status === 403) {
        if (result.msg !== 'Blocked user') {
          Alert.alert('登入錯誤', '請檢查用戶名/密碼是否正確', [
            {text: '確認', style: 'default'},
          ]);
          dispatch(failed('failed to login: incorrect email/password'));
          return;
        } else {
          Alert.alert(
            '登入錯誤',
            '你的帳戶異常,請聯絡我們: taggie1taggie@gmail.com',
            [{text: '確認', style: 'default'}],
          );
          dispatch(failed('failed to login: blocked user'));
          return;
        }
      }
      dispatch(loading());
      if (res.status !== 200) {
        if ((await res.json()).msg === 'Blocked user') {
          Alert.alert(
            '登入錯誤',
            '你的帳戶異常,請聯絡我們: taggie1taggie@gmail.com',
            [{text: '確認', style: 'default'}],
          );
          dispatch(failed('failed to login: blocked user'));
          return;
        }
        throw new Error(await res.json());
      }
      token = result?.data?.token;
      if (token) {
        await AsyncStorage.setItem('token', token);
      }
      console.log('msg', result.msg);
    } catch (error) {
      Alert.alert('登入錯誤', '請檢查用戶名/密碼是否正確', [
        {text: '確認', style: 'default'},
      ]);
      dispatch(failed('failed to login: ' + error.message));
      // dispatch(loaded());
      return;
    }

    // if everything goes right, not caught error and not returned yet
    dispatch(loadToken(token));
  };
}

export function updateToken() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth?.token;
      console.log('old token when update', userToken);
      const res = await api.get('/update-token', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result: {data: string} = await res.json();
      console.log('updated token', result);
      const payload: JWTPayload = jwt_decode(result.data);
      console.log('updated user thunk', payload);
      // console.log('result',result)
      // console.log('loadUserWishTags', result.data.wishTags);
      if (result.data) {
        dispatch(updatedToken(result.data));
        // dispatch(loaded());
        await AsyncStorage.setItem('token', result.data);
      } else {
        throw new Error('failed to update token');
      }
    } catch (error) {
      Alert.alert('更新Token時失敗', '請稍後重試', [
        {text: '確認', style: 'default'},
      ]);
      dispatch(failed('failed to update token: ' + error.msg + error.message));
      // dispatch(loaded());
      return;
    }
  };
}

export function confirmToBeRegStatThree() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      const userToken = getState().auth?.token;
      console.log('old token', userToken);
      const res = await api.get('/confirmFromTwoToThree', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result = await res.json();
      console.log('confirm stage 3', result);
      // const payload: JWTPayload = jwt_decode(result.data);
      // console.log('updated user thunk', payload);
      // console.log('result',result)
      // console.log('loadUserWishTags', result.data.wishTags);
      if (result.isSuccess === true) {
        console.log('token before pass', getState().auth?.token);
        dispatch(confirmedToBeRegStatThree());
        await dispatch(updateToken());
        // dispatch(loaded());
        RNRestart.Restart();
      } else {
        throw new Error('failed to proceed to stage 3');
      }
    } catch (error) {
      Alert.alert('更新狀態時失敗', '請稍後重試', [
        {text: '確認', style: 'default'},
      ]);
      // dispatch(loaded());
      dispatch(
        failed('failed to proceed to stage 3: ' + error.msg + error.message),
      );
      return;
    }
  };
}

export function signUp(input: RegistrationInput) {
  return async (dispatch: IRootThunkDispatch) => {
    let token: string;
    try {
      let res = await api.post('/signUp', input);
      if (res.status === 403) {
        Alert.alert('註冊錯誤', '電郵已被註冊', [
          {text: '確認', style: 'default'},
        ]);
        return;
      }
      dispatch(loading());
      if (res.status !== 200) {
        throw new Error(await res.json());
      }
      let result = await res.json();
      token = result?.data;
      if (token) {
        await AsyncStorage.setItem('token', token);
      }
      // dispatch(loaded());
    } catch (error) {
      Alert.alert('登入錯誤', '請檢查用戶名/密碼是否正確', [
        {text: '確認', style: 'default'},
      ]);
      dispatch(failed('failed to sign up: ' + error.message));
      // dispatch(loaded());
      return;
    }
    // if everything goes right, not caught error and not returned yet
    dispatch(loadToken(token));
  };
}
