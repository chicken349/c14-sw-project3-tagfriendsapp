import {
  failed,
  loadedChatroom,
  loadedChatroomMessages,
  postedChatroomMessage,
  readMessagesAlready,
  gotUnreadCount,
  updatedUnreadCount,
  // loading,
  loadingChatrooms,
  updatedRoomId,
} from './action';
import {IRootThunkDispatch, IRootState} from '../store';
import {api} from '../../helpers/api';
// import {Alert} from 'react-native';
import {ChatroomDetail, ChatroomMessage} from '../chatroom/state';
// import {loaded, loading} from '../loading/action';
// import io from 'socket.io-client';

export function loadChatroom() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loadingChatrooms());
    try {
      const userToken = getState().auth.token;
      const res = await api.get('/relations', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: ChatroomDetail[];
      } = await res.json();
      console.log('chatroom result', result);
      dispatch(loadedChatroom(result.data));
    } catch (error) {
      // Alert.alert('讀取聊天室時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to get chatroom: ' + error.msg + error.message));
      return;
    }
  };
}

export function loadChatroomMessages(room_id: string) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(updatedRoomId(room_id));
    try {
      const userToken = getState().auth.token;
      const res = await api.post('/chatroom', {roomId: room_id}, userToken);
      if (res.status !== 201) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: ChatroomMessage[];
      } = await res.json();
      // console.log('chatroom message result', result);
      dispatch(loadedChatroomMessages(result.data));
      // dispatch(readMessages(room_id));
    } catch (error) {
      // Alert.alert('讀取聊天室信息時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(
        failed('failed to get chatroom messages: ' + error.msg + error.message),
      );
      return;
    }
  };
}

export function postChatroomMessage(roomId: string, message: string) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(updatedRoomId(roomId));
    try {
      const userToken = getState().auth.token;
      // const messages = getState().chatroom.messages;
      const res = await api.post('/sendMessage', {roomId, message}, userToken);
      if (res.status !== 201) {
        throw await res.json();
      }
      // const result: {
      //   isSuccess: boolean;
      //   data: string;
      // } = await res.json();
      // console.log('post chatroom message result', result);
      dispatch(postedChatroomMessage());
    } catch (error) {
      // Alert.alert('發送聊天室信息時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(
        failed(
          'failed to send chatroom messages: ' + error.msg + error.message,
        ),
      );
      return;
    }
  };
}

export function readMessages(roomId: string) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(updatedRoomId(roomId));
    try {
      const userToken = getState().auth.token;
      // const chatrooms = getState().chatroom.chatroom;
      const res = await api.post('/readMessage', {roomId}, userToken);
      if (res.status !== 201) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {count: string};
      } = await res.json();
      console.log('chatroom message read already', result);
      dispatch(readMessagesAlready(parseInt(result.data.count, 10)));
      // dispatch(
      //   loadedChatroom([
      //     ...chatrooms!.map(room =>
      //       room.room_id === roomId ? {...room, unread: '0'} : room,
      //     ),
      //   ]),
      // );
    } catch (error) {
      // Alert.alert('已讀聊天室信息時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(
        failed(
          'failed to make chatroom list read to 0 and db to read: ' +
            error.msg +
            error.message,
        ),
      );
      return;
    }
  };
}

export function getUnreadCount() {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    // dispatch(loading());
    try {
      const userToken = getState().auth.token;
      const res = await api.get('/totalUnread', userToken);
      if (res.status !== 200) {
        throw await res.json();
      }
      const result: {
        isSuccess: boolean;
        data: {count: string};
      } = await res.json();
      console.log('unread result', parseInt(result.data.count, 10));
      dispatch(gotUnreadCount(parseInt(result.data.count, 10)));
    } catch (error) {
      // Alert.alert('讀取未讀信息時失敗', '請稍後重試', [
      //   {text: '確認', style: 'default'},
      // ]);
      dispatch(failed('failed to get unread: ' + error.msg + error.message));
      return;
    }
  };
}

export function updateUnreadCount(count: number) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    // dispatch(loading());
    try {
      const unreadCount = getState().chatroom.unread;
      const newUnreadCount = unreadCount ? unreadCount + count : count;
      dispatch(updatedUnreadCount(newUnreadCount));
    } catch (error) {
      dispatch(
        failed(
          'failed to get new count in background: ' + error.msg + error.message,
        ),
      );
      return;
    }
  };
}

// export function updateChatroomMessageToRead(roomId: string) {
//   return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
//     dispatch(loading());
//     try {
//       const messages = getState().chatroom.messages;

//       dispatch(
//         loadedChatroomMessages([
//           ...messages!.map(msg =>
//             msg.roomId === roomId ? {...msg, status: 'read'} : msg,
//           ),
//         ]),
//       );
//     } catch (error) {
//       dispatch(
//         failed(
//           'failed to make chatroom messages read: ' + error.msg + error.message,
//         ),
//       );
//     }
//   };
// }
