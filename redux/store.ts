import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose,
  AnyAction,
  CombinedState,
} from 'redux';
// import logger from 'redux-logger';
import thunk, {ThunkDispatch} from 'redux-thunk';
import {AuthState} from './auth/state';
import {AuthAction} from './auth/action';
import {authReducer} from './auth/reducer';
import {MatchState} from './match/state';
import {matchReducer} from './match/reducer';
import {MatchAction} from './match/action';
import {UserPhotosState} from './userPhotos/state';
import {UserPhotosAction} from './userPhotos/action';
import {UserPhotosReducer} from './userPhotos/reducer';
import {TagsState} from './tags/state';
import {TagsAction} from './tags/action';
import {TagsReducer} from './tags/reducer';
import {SocketState} from './socket/state';
import {SocketAction} from './socket/action';
import {SocketReducer} from './socket/reducer';
import {MapState} from './map/state';
import {MapAction} from './map/action';
import {mapReducer} from './map/reducer';
import {ChatroomState} from './chatroom/state';
import {ChatroomAction} from './chatroom/action';
import {ChatroomReducer} from './chatroom/reducer';
import {UserProfileState} from './userProfile/state';
import {UserProfileAction} from './userProfile/action';
import {UserProfileReducer} from './userProfile/reducer';
import {ReportState} from './report/state';
import {ReportAction} from './report/action';
import {reportReducer} from './report/reducer';
import {QuestionnaireState} from './questionnaire/state';
import {QuestionnaireAction} from './questionnaire/action';
import {QuestionnaireReducer} from './questionnaire/reducer';
import {LoadingState} from './loading/state';
import {LoadingAction} from './loading/action';
import {LoadingReducer} from './loading/reducer';
import {CoinRecordReducer} from './coin/reducer';
import {CoinRecordState} from './coin/state';
import {CoinRecordAction} from './coin/action';
import {ScheduleState} from './schedule/state';
import {ScheduleAction} from './schedule/action';
import {ScheduleReducer} from './schedule/reducer';
import {SuggestionState} from './suggestion/state';
import {SuggestionAction} from './suggestion/action';
import {SuggestionReducer} from './suggestion/reducer';
import {StripeState} from './stripe/state';
import {StripeAction} from './stripe/action';
import {StripeReducer} from './stripe/reducer';
import {FriendProfileState} from './friendProfile/state';
import {FriendProfileAction} from './friendProfile/action';
import {FriendProfileReducer} from './friendProfile/reducer';
import {crushReducer} from './crush/reducer';
import {CrushState} from './crush/state';
import {CrushAction} from './crush/action';

export type IRootState = {
  auth: AuthState;
  match: MatchState;
  userPhotos: UserPhotosState;
  tags: TagsState;
  socket: SocketState;
  map: MapState;
  chatroom: ChatroomState;
  userProfile: UserProfileState;
  report: ReportState;
  questionnaire: QuestionnaireState;
  coinRecord: CoinRecordState;
  schedule: ScheduleState;
  loading: LoadingState;
  suggestion: SuggestionState;
  stripe: StripeState;
  friendProfile: FriendProfileState;
  crush: CrushState;
};

export type IRootAction =
  | AuthAction
  | MatchAction
  | UserPhotosAction
  | SocketAction
  | TagsAction
  | ChatroomAction
  | UserProfileAction
  | MapAction
  | ReportAction
  | LoadingAction
  | CoinRecordAction
  | ScheduleAction
  | SuggestionAction
  | QuestionnaireAction
  | StripeAction
  | CrushAction
  | FriendProfileAction;

export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;

const appReducer = combineReducers<IRootState>({
  auth: authReducer,
  match: matchReducer,
  userPhotos: UserPhotosReducer,
  tags: TagsReducer,
  socket: SocketReducer,
  map: mapReducer,
  chatroom: ChatroomReducer,
  userProfile: UserProfileReducer,
  report: reportReducer,
  questionnaire: QuestionnaireReducer,
  coinRecord: CoinRecordReducer,
  schedule: ScheduleReducer,
  loading: LoadingReducer,
  suggestion: SuggestionReducer,
  stripe: StripeReducer,
  friendProfile: FriendProfileReducer,
  crush: crushReducer,
});

// declare global {
//   /* tslint:disable:interface-name */
//   interface Window {
//     __REDUX_DEVTOOLS_EXTENSION__: any;
//     __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
//   }
// }

const rootReducer = (
  state: CombinedState<IRootState> | undefined,
  action: AnyAction,
) => {
  // when a logout action is dispatched it will reset redux state
  if (action.type === '@@Auth/logout') {
    console.log('clearing state');
    state = undefined;
  }

  return appReducer(state, action);
};

const composeEnhancers =
  /* window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || */ compose;

const rootEnhancer = composeEnhancers(
  // applyMiddleware(logger),
  applyMiddleware(thunk),
  // applyMiddleware(logger),
  // applyMiddleware(routerMiddleware(history)),
);

let store = createStore<IRootState, IRootAction, {}, {}>(
  rootReducer,
  rootEnhancer,
);

export default store;
