export type CrushFriendInfo = {
  username: string;
  picture: {url: string};
  room: string;
  userId: number;
};

export type CrushHappenedFriendAndUserInfo = {
  firstMatchUser: CrushFriendInfo;
  lateMatchUser: CrushFriendInfo;
};

export type CrushState =
  | {
      status: 'error';
      msg: string;
      crushHappenedFriend?: CrushFriendInfo;
    }
  | {
      status: 'crushHappened';
      msg?: string;
      crushHappenedFriend: CrushFriendInfo;
    }
  | {
      status: 'crushHappenHandled';
      msg?: string;
      crushHappenedFriend?: CrushFriendInfo;
    };

export const initialState: CrushState = {
  status: 'crushHappenHandled',
};
