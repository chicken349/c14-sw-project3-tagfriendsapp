import {CrushState} from './state';
import {initialState} from './state';
import {CrushAction} from './action';

export const crushReducer = (
  state: CrushState = initialState,
  action: CrushAction,
): CrushState => {
  switch (action.type) {
    case '@@Crush/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };
    case '@@Crush/crushHappened':
      return {
        ...state,
        status: 'crushHappened',
        crushHappenedFriend: action.crushHappenedFriend,
      };
    case '@@Crush/crushHappenHandled':
      return {
        ...state,
        status: 'crushHappenHandled',
      };
    default:
      return state;
  }
};
