// import {Alert} from 'react-native';
import {api} from '../../helpers/api';
import {IRootState, IRootThunkDispatch} from '../store';
import {failed, loadFriendProfile, loaded, loading} from './action';

export function loadFriendProfileThunk(friend_id: number, profilePic: string) {
  return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
    dispatch(loading());
    try {
      let token = getState().auth.token;
      console.log('pass fd id: ', friend_id);
      let res = await api.post('/getFriendProfile', {friend_id}, token);
      if (res.status !== 200) {
        throw await res.json();
      }
      let result = await res.json();
      console.log('friend profile: ', result);
      dispatch(loaded());
      dispatch(
        loadFriendProfile(
          {...result.data, profilePic: profilePic},
          result.coordinate,
          result.text_description,
        ),
      );
    } catch (e) {
      dispatch(failed('get friend profile failed' + e.msg + e.message));
      dispatch(loaded());
      // Alert.alert('網絡連接失敗', '請重新檢查網絡', [
      //   {text: '確認', style: 'default'},
      // ]);
      return;
    }
  };
}
