import {FriendProfile, ILocation} from '../../helpers/types';

export type FriendProfileState =
  | {
      status: 'ready';
      msg?: string;
      friendProfile: FriendProfile;
      coordinate: ILocation;
      text_description: string;
    }
  | {
      status: 'error';
      msg: string;
      friendProfile?: FriendProfile;
      coordinate?: ILocation;
      text_description?: string;
    }
  | {
      status: 'loading';
      msg?: string;
      friendProfile?: FriendProfile;
      coordinate?: ILocation;
      text_description?: string;
    }
  | {
      status: 'loaded';
      msg?: string;
      friendProfile?: FriendProfile;
      coordinate?: ILocation;
      text_description?: string;
    };

export const initialState: FriendProfileState = {
  status: 'loading',
};
