import {FriendProfileAction} from './action';
import {FriendProfileState, initialState} from './state';

export const FriendProfileReducer = (
  state: FriendProfileState = initialState,
  action: FriendProfileAction,
): FriendProfileState => {
  switch (action.type) {
    case '@@FriendProfile/loadFriendProfile':
      return {
        ...state,
        status: 'ready',
        friendProfile: action.friendProfile,
        coordinate: action.coordinate,
        text_description: action.text_description,
      };

    case '@@FriendProfile/loaded':
      return {
        ...state,
        status: 'loaded',
      };

    case '@@FriendProfile/loading':
      return {
        ...state,
        status: 'loading',
      };

    case '@@FriendProfile/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg,
      };

    default:
      return state;
  }
};
