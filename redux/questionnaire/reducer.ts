import {QuestionnaireAction} from './action';
import {initialState, QuestionnaireState} from './state';

export const QuestionnaireReducer = (
  state: QuestionnaireState = initialState,
  action: QuestionnaireAction,
): QuestionnaireState => {
  switch (action.type) {
    case '@@Questionnaire/loading':
      return {
        ...state,
        status: 'loading',
      };
    case '@@Questionnaire/failed':
      return {
        ...state,
        status: 'failed',
        msg: action.msg,
      };

    case '@@Questionnaire/loaded':
      return {
        ...state,
        status: 'loaded',
      };

    default:
      return state;
  }
};
