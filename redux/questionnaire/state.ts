export type QuestionnaireState =
  | {
      status: 'failed';
      msg: string;
    }
  | {
      status: 'loaded';
      msg?: string;
    }
  | {
      status: 'loading';
      msg?: string;
    };

export const initialState: QuestionnaireState = {
  status: 'loading',
};
