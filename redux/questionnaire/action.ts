export function failed(msg: string) {
  console.log('failed questionnaire action', msg);
  return {
    type: '@@Questionnaire/failed' as const,
    msg,
  };
}

export function loading() {
  return {
    type: '@@Questionnaire/loading' as const,
  };
}

export function loaded() {
  return {
    type: '@@Questionnaire/loaded' as const,
  };
}

export type QuestionnaireAction =
  | ReturnType<typeof failed>
  | ReturnType<typeof loaded>
  | ReturnType<typeof loading>;
