/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Button} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import {isAndroid} from '../../utils/platform';

export const DateTimePickerComponent = ({
  date,
  show,
  onChange,
  showMode,
}: any) => {
  // const [date, setDate] = useState(props?.date);
  // // const [mode, setMode] = useState('date');
  // const [show, setShow] = useState(isAndroid ? false : true);

  // const onChange = (event: any, selectedDate: any) => {
  //   const currentDate = selectedDate || date;
  //   setShow(!isAndroid);
  //   setDate(currentDate);
  // };

  // const showMode = (/* currentMode: any */) => {
  //   setShow(true);
  //   // setMode(currentMode);
  // };

  // const showDatepicker = () => {
  //   showMode('date');
  // };

  // const showTimepicker = () => {
  //   showMode('time');
  // };

  return (
    <View>
      {isAndroid && (
        <View>
          <Button onPress={showMode} title={new Date(date).toDateString()} />
        </View>
      )}
      {/* <View>
        <Button onPress={showTimepicker} title="Show time picker!" />
      </View> */}
      {show && (
        <DateTimePicker
          style={{width: 150}}
          testID="dateTimePicker"
          value={date}
          // mode={mode as any}
          is24Hour={true}
          display="default"
          onChange={onChange}
          maximumDate={new Date(Date.now() - 1000 * 60 * 60 * 24 * 365.25 * 17)}
          minimumDate={new Date(1900, 1, 1)}
        />
      )}
    </View>
  );
};
