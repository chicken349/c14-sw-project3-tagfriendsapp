import React, {useState} from 'react';
import {useCallback} from 'react';
import {useEffect} from 'react';
import {
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  StyleSheet,
  TextInput,
  View,
  Button,
  Alert,
  Text,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {resumeNormal} from '../../redux/report/action';
import {submitReportThunk} from '../../redux/report/thunk';
import {IRootState} from '../../redux/store';
import {isAndroid} from '../../utils/platform';
// import LoadingComponent from '../utils/LoadingComponent';

const ReportScreen = ({navigation, route}: any) => {
  // const loadingStatus = useSelector(
  //   (state: IRootState) => state.report.status,
  // );
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const reportStatus = useSelector((state: IRootState) => state.report.status);
  const dispatch = useDispatch();
  const friend_id = route?.params?.friend_id;
  let name = null;
  if (friend_id) {
    name = route?.params?.name;
  }

  useEffect(() => {
    if (reportStatus === 'reported') {
      setTitle('');
      setContent('');
      dispatch(resumeNormal());
    }
  }, [dispatch, reportStatus]);

  const submitReport = useCallback(() => {
    if (title === '') {
      Alert.alert('提交錯誤', '請輸入標題', [{text: '確認', style: 'default'}]);
      return;
    }
    if (content === '') {
      Alert.alert('提交錯誤', '請輸入報告內容', [
        {
          text: '確認',
          style: 'default',
        },
      ]);
      return;
    }
    dispatch(
      submitReportThunk(
        {
          title: title,
          content: content,
          targetID: friend_id,
        },
        navigation,
      ),
    );
  }, [content, dispatch, friend_id, navigation, title]);

  // if (loadingStatus === 'loading') {
  //   return <LoadingComponent />;
  // }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.inner}>
          <TextInput
            style={styles.input}
            onChangeText={setTitle}
            value={title}
            placeholder="標題"
          />
          {name !== null ? <Text style={styles.input}>{name}</Text> : null}
          <View style={styles.OuterTextAreaContainer}>
            <View style={styles.textAreaContainer}>
              <TextInput
                style={styles.textArea}
                placeholder="報告內容"
                placeholderTextColor="grey"
                onChangeText={setContent}
                multiline={true}
                value={content}
              />
            </View>
          </View>
          <View style={styles.btnContainer}>
            <Button title="提交" onPress={() => submitReport()} />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 12,
    paddingHorizontal: 15,
    color: "black",
  },
  content: {
    height: 320,
    margin: 12,
    borderWidth: 1,
    borderRadius: 10,
  },
  btnContainer: {
    // backgroundColor: isAndroid ? 'white' : 'transparent',
    marginTop: 12,
    marginLeft: 120,
    marginRight: 120,
  },
  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 15,
    // height: 100,
  },
  textInput: {
    bottom: 0,
    flex: 1,
    marginRight: 15,
    backgroundColor: '#DCDCDC',
    paddingHorizontal: 20,
    color: '#4f4f4f',
    borderRadius: 20,
    paddingTop: 7,
    borderColor: 'transparent',
  },
  container: {
    flex: 1,
  },
  inner: {
    padding: 24,
    flex: 1,
    justifyContent: 'space-around',
  },
  header: {
    fontSize: 36,
  },
  OuterTextAreaContainer: {
    borderWidth: 1,
    padding: 5,
    borderRadius: 10,
    margin: 12,
    flex: 1,
  },
  textAreaContainer: {
    borderWidth: isAndroid ? 0 : 1,
    borderColor: 'transparent',
  },
  textArea: {
    justifyContent: 'flex-start',
    textAlignVertical: 'top',
    padding: 10,
    color: 'black',
  },
});

export default ReportScreen;
