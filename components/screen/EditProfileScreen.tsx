/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Platform,
  Animated,
  Modal,
} from 'react-native';
import {IRootState} from '../../redux/store';
import {useDispatch, useSelector} from 'react-redux';

import {useTheme} from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import ImagePicker from 'react-native-image-crop-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {isAndroid} from '../../utils/platform';
import {genders, ImageUpload} from '../../helpers/types';
import FastImage from 'react-native-fast-image';
import {S3Link} from '../../helpers/api';
import {DateTimePickerComponent} from '../utils/DateTimePicker';
import {isDate, reverseTimeDifference} from '../../helpers/date';
import {UserEditable} from '../../redux/userProfile/state';
import {
  editUserProfile,
  editUserProfilePic,
} from '../../redux/userProfile/thunk';
import {confirmToBeRegStatThree} from '../../redux/auth/thunk';
import LoadingComponent from './Loading/LoadingComponent';

const EditProfileScreen = ({navigation}: any) => {
  const loadingStatus = useSelector(
    (state: IRootState) => state.userProfile.status,
  );
  const user = useSelector((state: IRootState) => state.auth.user);
  const tokenProfilePic = useSelector(
    (state: IRootState) => state.auth.user?.profilePic,
  );
  const originalProfilePicNew = useRef<ImageUpload | undefined>();
  const [currentProfilePic, setCurrentProfilePic] = useState<ImageUpload[]>();
  const [currentProfile, setCurrentProfile] = useState<string | undefined>();
  const userId = useSelector((state: IRootState) => state.auth?.user?.id);
  const userDetail = useSelector(
    (state: IRootState) => state.userProfile.userProfile?.info,
  );
  const profilePics = useSelector(
    (state: IRootState) => state.userProfile.userProfilePic?.userProfilePic,
  );
  const [userEditable, setUserEditable] = useState<UserEditable>({
    username: userDetail?.username || undefined,
    gender: (userDetail &&
      genders.find(item => item.DB === userDetail.gender)) || {
      DB: 'male',
      pic: 'gender-male',
    },
    birthday:
      (userDetail && new Date(userDetail.birthday)) || new Date(1598051730000),
    text_description: userDetail?.text_description || undefined,
  });
  const [messageBarHeight, setMessageBarHeight] = useState(40);
  const [toSubmitEditProfile, setToSubmitEditProfile] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [show, setShow] = useState(isAndroid ? false : true);

  const {colors} = useTheme();
  const dispatch = useDispatch();

  useEffect(() => {
    if (userDetail) {
      setUserEditable({
        username: userDetail?.username || undefined,
        gender: (userDetail &&
          genders.find(item => item.DB === userDetail.gender)) || {
          DB: 'male',
          pic: 'gender-male',
        },
        birthday:
          (userDetail && new Date(userDetail.birthday)) ||
          new Date(1598051730000),
        text_description: userDetail?.text_description || undefined,
      });
    }
  }, [userDetail]);

  useEffect(() => {
    if (toSubmitEditProfile) {
      dispatch(editUserProfile(userEditable));
      setToSubmitEditProfile(false);
      if (user?.registration_status === 2) {
        dispatch(confirmToBeRegStatThree());
      } else {
        navigation.goBack();
      }
    }
  }, [
    dispatch,
    toSubmitEditProfile,
    userEditable,
    navigation,
    user?.registration_status,
    user,
  ]);

  const onChange = useCallback(
    (event: any, selectedDate: any) => {
      const currentDate = isDate(selectedDate)
        ? selectedDate
        : userEditable.birthday;
      setShow(!isAndroid);
      setUserEditable(prev => ({
        ...prev,
        birthday: reverseTimeDifference(currentDate),
      }));
    },
    [userEditable.birthday],
  );

  const showMode = useCallback(() => {
    setShow(true);
  }, []);

  useEffect(() => {
    if (currentProfilePic) {
      dispatch(editUserProfilePic(currentProfilePic));
    }
  }, [currentProfilePic, dispatch]);

  useEffect(() => {
    if (profilePics && profilePics.length > 1) {
      setCurrentProfile(
        profilePics.filter(pic => pic.description === 'profile')[0].url,
      );
    }
  }, [profilePics]);

  const editedUserProfile = useCallback((type: string, data: string | {}) => {
    setUserEditable(prev => ({...prev, [type]: data}));
  }, []);

  const cropPhotosForProfile = useCallback(
    (imagePath: string) => {
      ImagePicker.openCropper({
        path: imagePath,
        mediaType: 'photo',
        width: 300,
        height: 300,
        cropperCircleOverlay: true,
        compressImageQuality: 1,
        forceJpg: true,
      })
        .then(croppedImage => {
          if (croppedImage) {
            console.log(croppedImage);
            setCurrentProfilePic([
              originalProfilePicNew.current!,
              {
                uri: croppedImage.path,
                name: `profile-${userId}`,
                type: croppedImage.mime,
              },
            ]);
          }
        })
        .catch(e => console.log('crop failed', e));
    },
    [userId],
  );

  const cropPhotosForOriginalProfile = useCallback(
    (imagePath: string) => {
      ImagePicker.openCropper({
        path: imagePath,
        mediaType: 'photo',
        width: 600,
        height: 1000,
        compressImageQuality: 0.8,
        forceJpg: true,
      })
        .then(croppedImage => {
          if (croppedImage) {
            originalProfilePicNew.current = {
              uri: croppedImage.path,
              name: `original-profile-${userId}`,
              type: croppedImage.mime,
            };
            console.log('iosLibImage', croppedImage);
          }
          cropPhotosForProfile(imagePath);
        })
        .catch(e => console.log('crop failed', e));
    },
    [cropPhotosForProfile, userId],
  );

  const takePhotoFromCameraForProfile = useCallback(() => {
    launchCamera(
      {
        mediaType: 'photo',
      },
      res => {
        if (res?.assets && res?.assets[0]?.uri) {
          cropPhotosForOriginalProfile(res?.assets[0]?.uri);
        }

        console.log(res);
      },
    );
  }, [cropPhotosForOriginalProfile]);

  const choosePhotoFromLibraryForProfile = useCallback(() => {
    if (!isAndroid) {
      ImagePicker.openPicker({
        mediaType: 'photo',
      })
        .then(image => {
          cropPhotosForOriginalProfile(image.path);
        })
        .catch(() => {
          console.log('user did not allow camera / no camera');
        });
    } else {
      launchImageLibrary(
        {
          mediaType: 'photo',
        },
        res => {
          if (res?.assets && res?.assets[0]?.uri) {
            cropPhotosForOriginalProfile(res?.assets[0]?.uri);
          }

          console.log(res);
        },
      );
    }
  }, [cropPhotosForOriginalProfile]);

  const fall = new Animated.Value(1);

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <View style={styles.container}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <TouchableOpacity
          style={styles.centeredView}
          onPress={() => setModalVisible(!modalVisible)}>
          <View style={styles.modalView}>
            {genders.map((gender, index) => (
              <TouchableOpacity
                key={index}
                style={{marginVertical: 10}}
                onPress={() => editedUserProfile('gender', gender)}>
                <MaterialCommunityIcons
                  name={gender.pic}
                  color={
                    userEditable && userEditable.gender.DB === gender.DB
                      ? 'red'
                      : colors.text
                  }
                  size={20}
                />
              </TouchableOpacity>
            ))}
            <TouchableOpacity
              style={[styles.button, styles.buttonClose, {marginTop: 10}]}
              onPress={() => setModalVisible(false)}>
              <Text style={styles.textStyle}>確認</Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </Modal>
      <Animated.ScrollView
        style={{
          margin: 20,
          opacity: Animated.add(0.1, Animated.multiply(fall, 1.0)),
        }}
        showsVerticalScrollIndicator={false}>
        <View style={{alignItems: 'center'}}>
          <View style={styles.alignSelf}>
            <View style={styles.profileImage}>
              <FastImage
                source={
                  !tokenProfilePic && !currentProfile
                    ? require('../../src/image/profile.png')
                    : {
                        uri: tokenProfilePic
                          ? S3Link + tokenProfilePic
                          : S3Link + currentProfile,
                        priority: FastImage.priority.high,
                      }
                }
                style={{height: 200, width: 200}}
              />
            </View>
            <TouchableOpacity
              style={styles.editLeft}
              onPress={() => takePhotoFromCameraForProfile()}>
              <MaterialIcons
                name="photo-camera"
                size={20}
                color="#DFD8C8"
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.editRight}
              onPress={() => choosePhotoFromLibraryForProfile()}>
              <MaterialCommunityIcons
                name="circle-edit-outline"
                size={20}
                color="#DFD8C8"
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={{marginTop: 50}}>
          <View style={styles.action}>
            <FontAwesome name="user-o" color={colors.text} size={20} />
            <TextInput
              placeholder="你的用戶名"
              placeholderTextColor="#666666"
              autoCorrect={false}
              style={[
                styles.textInput,
                {
                  color: colors.text,
                },
              ]}
              value={userEditable && userEditable.username}
              onChangeText={text => editedUserProfile('username', text)}
            />
          </View>
          <View style={[styles.action, {marginTop: isAndroid ? 0 : 10}]}>
            <FontAwesome name="birthday-cake" color={colors.text} size={20} />
            <View style={{marginLeft: 10, transform: [{translateY: -5}]}}>
              <DateTimePickerComponent
                date={userEditable.birthday}
                show={show}
                onChange={onChange}
                showMode={showMode}
              />
            </View>
          </View>
          <View
            style={[
              styles.action,
              {marginTop: isAndroid ? 0 : -5, width: 100},
            ]}>
            <SimpleLineIcons
              name={
                userDetail && userDetail.gender === 'female'
                  ? 'user-female'
                  : 'user'
              }
              color={colors.text}
              size={20}
            />
            <TouchableOpacity
              style={{marginLeft: 10}}
              onPress={() => setModalVisible(true)}>
              <MaterialCommunityIcons
                name={userEditable.gender.pic}
                color={colors.text}
                size={20}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.action}>
            <MaterialIcons name="speaker-notes" color={colors.text} size={20} />
            <TextInput
              placeholder="你的簡介"
              placeholderTextColor="#666666"
              keyboardType="email-address"
              multiline
              autoCorrect={false}
              spellCheck={false}
              style={[
                styles.textInput,
                {
                  marginTop: isAndroid ? -15 : -5,
                  color: colors.text,
                  height: messageBarHeight + (isAndroid ? 7.5 : 25),
                  borderLeftWidth: 1,
                  borderColor: 'transparent',
                },
              ]}
              onContentSizeChange={e =>
                setMessageBarHeight(
                  Math.min(
                    e.nativeEvent.contentSize.height,
                    isAndroid ? 140 : 120,
                  ),
                )
              }
              value={userEditable && userEditable.text_description}
              onChangeText={text => editedUserProfile('text_description', text)}
            />
          </View>
          <View style={{marginTop: 50}}>
            <TouchableOpacity
              style={styles.commandButton}
              onPress={() => {
                setToSubmitEditProfile(true);
                console.log('click');
              }}>
              <Text style={styles.panelButtonTitle}>確認</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Animated.ScrollView>
    </View>
  );
};

export default EditProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  alignSelf: {
    alignSelf: 'center',
  },
  profileImage: {
    width: 200,
    height: 200,
    borderRadius: 100,
    overflow: 'hidden',
  },
  editLeft: {
    backgroundColor: '#41444B',
    position: 'absolute',
    bottom: 0,
    left: 10,
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  editRight: {
    backgroundColor: '#41444B',
    position: 'absolute',
    bottom: 0,
    right: 10,
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  commandButton: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: '#2196F3',
    alignItems: 'center',
    marginTop: 10,
  },
  panel: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
  },
  header: {
    backgroundColor: '#FFFFFF',
    shadowColor: '#333333',
    shadowOffset: {width: -1, height: -3},
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#FF6347',
    alignItems: 'center',
    marginVertical: 7,
  },
  panelButtonTitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
