/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useLayoutEffect} from 'react';
import {View, ScrollView, StyleSheet, TouchableOpacity} from 'react-native';
import {Title, Caption, Text, TouchableRipple} from 'react-native-paper';
import {logout as logoutThunk} from '../../redux/auth/thunk';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
// import {useEffect} from 'react';
// import {
//   loadUserProfile,
//   loadUserProfilePic,
// } from '../../redux/userProfile/thunk';
import {S3Link} from '../../helpers/api';
import FastImage from 'react-native-fast-image';
import LoadingComponent from './Loading/LoadingComponent';
import {loadedUserProfile} from '../../redux/userProfile/action';
import CrushScreen from './Crush/CrushScreen';
// import {useState} from 'react';

// import {AuthContext} from '../context/context';

// import Share from 'react-native-share';

// const files = require('../assets/filesBase64');

const AVATAR_SIZE = 80;

const ProfileScreen = ({navigation}: any) => {
  const loadingStatus = useSelector(
    (state: IRootState) => state.userProfile.status,
  );
  const userDetail = useSelector(
    (state: IRootState) => state.userProfile.userProfile?.info,
  );
  const matchNum = useSelector(
    (state: IRootState) => state.userProfile.userProfile?.matchNum,
  );
  const crushStatus = useSelector((state: IRootState) => state.crush.status);
  const userProfile = useSelector(
    (state: IRootState) => state.userProfile?.userProfile,
  );
  // const loadingStatus = useSelector(
  //   (state: IRootState) => state.userProfile.status,
  // );

  const crushFriendInfo = useSelector(
    (state: IRootState) => state.crush.crushHappenedFriend,
  );
  const profilePic = useSelector(
    (state: IRootState) =>
      state.userProfile.userProfilePic?.userProfilePic.find(
        item => item.description === 'profile',
      )?.url,
  );
  // const [userProfilePic, setUserProfilePic] = useState(
  //   profilePic?.userProfilePic?.find(item => item.description === 'profile')
  //     ?.url,
  // );

  console.log('profilePic', profilePic);

  const dispatch = useDispatch();

  useLayoutEffect(() => {
    navigation.setOptions({
      title: '編輯資料',
      headerBackTitleVisible: false,
      headerRight: () => (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            // width: 80,
            marginRight: 20,
          }}>
          <TouchableOpacity>
            <MaterialCommunityIcons
              name="account-edit"
              size={25}
              // backgroundColor={colors.background}
              color="#333333"
              onPress={() => navigation.navigate('EditProfile')}
            />
          </TouchableOpacity>
        </View>
      ),
    });
  }, [navigation]);

  // useEffect(() => {
  //   dispatch(loadUserProfile());
  //   dispatch(loadUserProfilePic());
  // }, [dispatch]);

  useEffect(() => {
    if (loadingStatus !== 'loading' && loadingStatus !== 'error') {
      dispatch(loadedUserProfile(userProfile!));
    }
  }, [userDetail, dispatch, loadingStatus, userProfile]);

  const signOut = () => {
    dispatch(logoutThunk());
  };

  if (crushStatus === 'crushHappened' && crushFriendInfo) {
    return (
      <CrushScreen navigation={navigation} userPresent={crushFriendInfo} />
    );
  }

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <ScrollView style={[styles.container, {marginBottom: tabBarHeight}]}>
          <View style={styles.userInfoSection}>
            <View style={{flexDirection: 'row', marginTop: 15}}>
              <FastImage
                source={
                  profilePic
                    ? {
                        uri: S3Link + profilePic,
                        priority: FastImage.priority.high,
                      }
                    : require('../../src/image/profile.png')
                }
                style={{
                  width: AVATAR_SIZE,
                  height: AVATAR_SIZE,
                  borderRadius: AVATAR_SIZE,
                  // marginRight: SPACING / 2,
                }}
              />
              {/* <Avatar.Image
                source={{
                  uri: tokenProfilePic
                    ? S3Link + tokenProfilePic
                    : 'https://randomuser.me/api/portraits/men/1.jpg',
                }}
                size={80}
              /> */}

              <View style={{marginLeft: 20, justifyContent: 'center'}}>
                <Title style={[styles.title]}>
                  {userDetail && userDetail.username}
                </Title>
                <View style={styles.row}>
                  <Icon name="email" color="#777777" size={20} />
                  <Text style={{color: '#777777', marginLeft: 10}}>
                    {userDetail && userDetail.email}
                  </Text>
                </View>
                {/* <Caption style={styles.caption}>@j_doe</Caption> */}
              </View>
            </View>
          </View>

          <View style={styles.userInfoSection}>
            {/* <View style={styles.row}>
              <Icon name="map-marker-radius" color="#777777" size={20} />
              <Text style={{color: '#777777', marginLeft: 20}}>
                Kolkata, India
              </Text>
            </View> */}
            {/* <View style={styles.row}>
              <Icon name="phone" color="#777777" size={20} />
              <Text style={{color: '#777777', marginLeft: 20}}>
                +91-900000009
              </Text>
            </View> */}
          </View>

          <View style={styles.infoBoxWrapper}>
            <View
              style={[
                styles.infoBox,
                {
                  borderRightColor: '#dddddd',
                  borderRightWidth: 1,
                },
              ]}>
              <Title>{userDetail && userDetail.coins}</Title>
              <Caption>Taggie幣</Caption>
            </View>
            <View style={styles.infoBox}>
              <Title>{matchNum}</Title>
              <Caption>配對</Caption>
            </View>
          </View>

          <View style={styles.menuWrapper}>
            <TouchableRipple
              onPress={() => {
                navigation.navigate('UserTags');
              }}>
              <View style={styles.menuItem}>
                <MaterialIcons name="loyalty" color="#FF6347" size={25} />
                <Text style={styles.menuItemText}>你的Tag</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple
              onPress={() => {
                navigation.navigate('EditGallery');
              }}>
              <View style={styles.menuItem}>
                <MaterialIcons name="photo-library" color="#FF6347" size={25} />
                <Text style={styles.menuItemText}>相簿</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple
              onPress={() => {
                navigation.navigate('StripeGatewayScreen');
              }}>
              <View style={styles.menuItem}>
                <Icon name="credit-card" color="#FF6347" size={25} />
                <Text style={styles.menuItemText}>付款</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple
              onPress={() => {
                navigation.navigate('CoinHistory');
              }}>
              <View style={styles.menuItem}>
                <MaterialIcons
                  name="monetization-on"
                  color="#FF6347"
                  size={25}
                />
                <Text style={styles.menuItemText}>交易記錄</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple
              onPress={
                /* myCustomShare */ () => {
                  navigation.navigate('Report');
                }
              }>
              <View style={styles.menuItem}>
                <Octicons name="report" color="#FF6347" size={25} />
                <Text style={styles.menuItemText}>報告</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple
              onPress={() => {
                navigation.navigate('ScheduleDating');
              }}>
              <View style={styles.menuItem}>
                <Icon name="account-check-outline" color="#FF6347" size={25} />
                <Text style={styles.menuItemText}>你的日程</Text>
              </View>
            </TouchableRipple>
            <TouchableRipple onPress={signOut}>
              <View style={styles.menuItem}>
                <Ionicon name="person" color="#FF6347" size={25} />
                <Text style={styles.menuItemText}>登出</Text>
              </View>
            </TouchableRipple>
          </View>
        </ScrollView>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#efefef',
  },
  userInfoSection: {
    paddingHorizontal: 30,
    marginBottom: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    fontWeight: '500',
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  infoBoxWrapper: {
    borderBottomColor: '#dddddd',
    borderBottomWidth: 1,
    borderTopColor: '#dddddd',
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 100,
  },
  infoBox: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuWrapper: {
    marginTop: 10,
  },
  menuItem: {
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  menuItemText: {
    color: '#777777',
    marginLeft: 20,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },
});
