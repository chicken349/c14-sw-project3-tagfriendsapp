/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  StyleSheet,
  StatusBar,
  // Alert,
} from 'react-native';
// import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import {login as loginThunk} from '../../../redux/auth/thunk';
import {LoginInput} from '../../../helpers/types';
// import {logout as logoutThunk, login as loginThunk} from './redux/auth/thunk';
import {IRootState} from '../../../redux/store';

import {useTheme} from 'react-native-paper';

// import {AuthContext} from '../../context/context';

// import Users from '../../../model/users';
import {useDispatch, useSelector} from 'react-redux';
import LoadingComponent from '../Loading/LoadingComponent';
import Entypo from 'react-native-vector-icons/Entypo';

const SignInScreen = ({navigation}: any) => {
  const [data, setData] = React.useState({
    email: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
  });
  const authLoadingStatus = useSelector(
    (state: IRootState) => state.auth.status,
  );

  const {colors} = useTheme();

  const dispatch = useDispatch();

  const signIn = (signInData: LoginInput) => {
    if (signInData.email.length < 3) {
      if (signInData.password.length === 0) {
        setData({
          ...data,
          isValidUser: false,
          isValidPassword: false,
        });
      } else {
        setData({
          ...data,
          isValidUser: false,
        });
      }
      return;
    }
    if (signInData.password.length === 0) {
      setData({
        ...data,
        isValidPassword: false,
      });
      return;
    }
    if (data.check_textInputChange === false) {
      return;
    }
    dispatch(loginThunk(signInData));
  };

  const textInputChange = (val: any) => {
    if (val.trim().length === 0) {
      setData({
        ...data,
        email: val,
        check_textInputChange: false,
        isValidUser: false,
      });
    } else {
      if (val.includes('@') && val[val.length - 1] !== '@') {
        setData({
          ...data,
          email: val,
          check_textInputChange: true,
          isValidUser: true,
        });
      } else {
        setData({
          ...data,
          email: val,
          check_textInputChange: false,
          isValidUser: true,
        });
      }
    }
  };

  const handlePasswordChange = (val: any) => {
    if (val.trim().length >= 1) {
      setData({
        ...data,
        password: val,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false,
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const handleValidUser = (val: any) => {
    if (val.trim().length >= 1) {
      setData({
        ...data,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        isValidUser: false,
      });
    }
  };

  if (authLoadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>歡迎👏!</Text>
      </View>
      <View
        // animation="fadeInUpBig"
        style={[
          styles.footer,
          {
            backgroundColor: colors.background,
          },
        ]}>
        <Text
          style={[
            styles.text_footer,
            {
              color: colors.text,
            },
          ]}>
          電郵
        </Text>
        <View style={styles.action}>
          <FontAwesome name="user-o" color={colors.text} size={20} />
          <TextInput
            placeholder="你的電郵"
            placeholderTextColor="#666666"
            style={[
              styles.textInput,
              {
                color: colors.text,
              },
            ]}
            autoCapitalize="none"
            onChangeText={val => textInputChange(val)}
            onEndEditing={e => handleValidUser(e.nativeEvent.text)}
          />
          {data.check_textInputChange ? (
            <View /* animation="bounceIn" */>
              <Feather name="check-circle" color="green" size={20} />
            </View>
          ) : (
            !data.check_textInputChange &&
            data.email !== '' && (
              <View /* animation="bounceIn" */>
                <Entypo name="circle-with-cross" color="red" size={20} />
              </View>
            )
          )}
        </View>
        {data.isValidUser ? null : (
          <View /* animation="fadeInLeft"  duration={500} */>
            <Text style={styles.errorMsg}>請輸入電郵。</Text>
          </View>
        )}

        <Text
          style={[
            styles.text_footer,
            {
              color: colors.text,
              marginTop: 35,
            },
          ]}>
          密碼
        </Text>
        <View style={styles.action}>
          <Feather name="lock" color={colors.text} size={20} />
          <TextInput
            placeholder="你的密碼"
            placeholderTextColor="#666666"
            secureTextEntry={data.secureTextEntry ? true : false}
            style={[
              styles.textInput,
              {
                color: colors.text,
              },
            ]}
            autoCapitalize="none"
            onChangeText={val => handlePasswordChange(val)}
          />
          <TouchableOpacity
            onPress={updateSecureTextEntry}
            style={{height: 40, width: 40}}>
            {data.secureTextEntry ? (
              <Feather
                name="eye-off"
                color="grey"
                size={20}
                style={{alignSelf: 'flex-end'}}
              />
            ) : (
              <Feather
                name="eye"
                color="grey"
                size={20}
                style={{alignSelf: 'flex-end'}}
              />
            )}
          </TouchableOpacity>
        </View>
        {data.isValidPassword ? null : (
          <View>
            <Text style={styles.errorMsg}>請輸入完整密碼。</Text>
          </View>
        )}

        <View style={styles.button}>
          <TouchableOpacity
            style={styles.signIn}
            onPress={() => {
              signIn({
                email: data.email,
                password: data.password,
                lang: 'zh',
              });
            }}>
            <LinearGradient
              colors={['#08d4c4', '#01ab9d']}
              style={styles.signIn}>
              <Text
                style={[
                  styles.textSign,
                  {
                    color: '#fff',
                  },
                ]}>
                登入
              </Text>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigation.navigate('SignUpScreen')}
            style={[
              styles.signIn,
              {
                borderColor: '#009387',
                borderWidth: 1,
                marginTop: 15,
              },
            ]}>
            <Text
              style={[
                styles.textSign,
                {
                  color: '#009387',
                },
              ]}>
              註冊
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#009387',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
