/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState, useCallback} from 'react';
import {
  StyleSheet,
  Text,
  // TextInput,
  View,
  // ScrollView,
  Animated,
  TouchableOpacity,
  Dimensions,
  Platform,
  FlatList,
  // Button,
} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE, Region} from 'react-native-maps';

// import Ionicons from 'react-native-vector-icons/Ionicons';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import Fontisto from 'react-native-vector-icons/Fontisto';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';
import {useAnimatedRef} from 'react-native-reanimated';

// import { markers } from '../../model/mapData';
import {mapDarkStyle, mapStandardStyle} from '../../model/googleMapStyle';
// import StarRating from '../../model/StarRating';
import {useTheme} from '@react-navigation/native';
import FastImage from 'react-native-fast-image';
import {IRootState} from '../../redux/store';
import {useDispatch, useSelector} from 'react-redux';
import {S3Link} from '../../helpers/api';
import Modal from 'react-native-modal';
import {getWhoLikesU} from '../../redux/map/thunk';
import LoadingComponent from './Loading/LoadingComponent';
import CrushScreen from './Crush/CrushScreen';

const {width, height} = Dimensions.get('window');
const CARD_HEIGHT = 180;
const CARD_WIDTH = 150;
const SPACING = 10;

const WhoLikedYouScreen = ({navigation}: any) => {
  const theme = useTheme();
  const loadingStatus = useSelector((state: IRootState) => state.map.status);
  const dispatch = useDispatch();
  // const location = useSelector((state: IRootState) => state.match.location);
  // const [state] = useState();
  // const region = useSelector((state: IRootState) => state.map.region);
  const markers = useSelector((state: IRootState) => state.map.whoLikeU);
  const mapIndex = useRef(0);
  const selectedUserIndex = useRef<number | undefined>();
  // const mapAnimation = useRef(new Animated.Value(0)).current;
  const _map = useAnimatedRef<MapView>();
  const _flatList = useRef<FlatList<any> | null>(null);
  const [isModalVisible, setModalVisible] = useState(false);
  const selectedMarker = markers && markers[selectedUserIndex.current!];
  const crushStatus = useSelector((state: IRootState) => state.crush.status);
  const crushFriendInfo = useSelector(
    (state: IRootState) => state.crush.crushHappenedFriend,
  );
  const [currentClickedIndex, setCurrentClickedIndex] = useState(0);
  const [currentRegion, setCurrentRegion] = useState<Region | undefined>();

  useEffect(() => {
    dispatch(getWhoLikesU());
  }, [dispatch]);

  const scrollToActiveIndex = useCallback(
    (index: number) => {
      console.log('scroll', index);
      // setActiveIndex(index);
      console.log(
        'width',
        CARD_WIDTH,
        SPACING,
        width,
        CARD_WIDTH + SPACING - width / 2 + CARD_WIDTH / 2,
      );
      // thumbRef?.current?.scrollToIndex({
      //   index: index,
      //   animated: true,
      // });

      _flatList?.current?.scrollToOffset({
        offset:
          index * (CARD_WIDTH + SPACING * 2) -
          width / 2 +
          CARD_WIDTH / 2 +
          SPACING * 2,
        animated: true,
      });

      const coordinate = markers
        ? markers[index].coordinate
        : {latitude: 22.2876, longitude: 114.14816};
      setCurrentClickedIndex(index);

      if (currentRegion) {
        _map?.current?.animateToRegion(
          {
            ...coordinate,
            latitudeDelta:
              currentRegion.latitudeDelta > 0.04864195044303443
                ? 0.04864195044303443
                : currentRegion.latitudeDelta,
            longitudeDelta:
              currentRegion.longitudeDelta > 0.040142817690068
                ? 0.040142817690068
                : currentRegion.longitudeDelta,
          },
          200,
        );
      }
    },
    [_map, currentRegion, markers],
  );

  const onMarkerPress = useCallback(
    (index: number) => {
      console.log('google map index', index);
      mapIndex.current = index;

      scrollToActiveIndex(index);
    },
    [scrollToActiveIndex],
  );

  const onRegionChangeComplete = useCallback((mapRegion: Region) => {
    setCurrentRegion(mapRegion);
  }, []);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const onPhotoPress = (index: number) => {
    selectedUserIndex.current = index;
    toggleModal();
    // return (
    //   <View style={{flex: 1}}>
    //     <Text>HAHA</Text>
    //     <Button title="Show modal" onPress={toggleModal} />
    //   </View>
    // );
  };

  if (crushStatus === 'crushHappened' && crushFriendInfo) {
    return (
      <CrushScreen navigation={navigation} userPresent={crushFriendInfo} />
    );
  }

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <View style={styles.container}>
          <MapView
            ref={_map}
            onRegionChangeComplete={onRegionChangeComplete}
            initialRegion={
              markers && markers.length > 0
                ? {
                    latitude: markers[0].coordinate.latitude,
                    longitude: markers[0].coordinate.longitude,
                    latitudeDelta: 0.004864195044303443,
                    longitudeDelta: 0.0040142817690068,
                  }
                : {
                    latitude: 25.2860375,
                    longitude: 114.1486955,
                    latitudeDelta: 0.004864195044303443,
                    longitudeDelta: 0.0040142817690068,
                  }
            }
            style={styles.container}
            provider={PROVIDER_GOOGLE}
            customMapStyle={theme.dark ? mapDarkStyle : mapStandardStyle}>
            {markers &&
              markers.length > 0 &&
              markers.map((marker, index) => {
                const scaleStyle = {
                  transform: [
                    {
                      scale: index === currentClickedIndex ? 2 : 1,
                    },
                  ],
                };
                return (
                  <Marker
                    key={index}
                    coordinate={marker.coordinate}
                    onPress={() => onMarkerPress(index)}>
                    <Animated.View style={[styles.markerWrap]}>
                      <Animated.Image
                        source={require('../../src/image/map_marker.png')}
                        style={[styles.marker, scaleStyle]}
                        resizeMode="cover"
                      />
                    </Animated.View>
                  </Marker>
                );
              })}
          </MapView>
          {markers && markers.length === 0 && (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'rgba(255,255,255, 0.5)',
                position: 'absolute',
                bottom: 0,
                top: 0,
                left: 0,
                right: 0,
              }}>
              <Text style={{fontSize: 20, color: '#000'}}>
                🙇🏻‍♂️暫時未有人Like你🙇🏻‍♀️
              </Text>
            </View>
          )}
          {markers && markers.length > 0 && (
            <View
              style={{
                position: 'absolute',
                bottom: 0,
                left: 0,
                right: 0,
                paddingVertical: 10,
                height: 350,
                width: width,
                backgroundColor: 'rgba(255, 255, 255, 0.5)',
              }}>
              <Text style={{fontSize: 20, color: '#000', textAlign: 'center'}}>
                想和你結識的有緣人:
              </Text>
              <FlatList
                ref={_flatList}
                horizontal
                data={markers}
                keyExtractor={item => (item.userId + Math.random()).toString()}
                showsHorizontalScrollIndicator={false}
                style={styles.scrollView}
                renderItem={({item: marker, index}) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        scrollToActiveIndex(index);
                      }}
                      style={[
                        styles.card,
                        {marginBottom: tabBarHeight},
                        index === currentClickedIndex
                          ? {
                              borderWidth: 1,
                              borderColor: 'red',
                              transform: [{translateY: -20}],
                            }
                          : {},
                      ]}>
                      <TouchableOpacity
                        onPress={() => {
                          onPhotoPress(index);
                        }}>
                        <FastImage
                          source={{
                            uri: S3Link + marker.profilePic,
                            priority: FastImage.priority.high,
                          }}
                          style={styles.cardImage}
                          resizeMode="contain"
                        />
                      </TouchableOpacity>
                      <View style={styles.textContent}>
                        <Text numberOfLines={2} style={styles.cardtitle}>
                          {marker.username}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            </View>
          )}
          <Modal isVisible={isModalVisible}>
            <TouchableOpacity
              onPress={toggleModal}
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              {selectedMarker ? (
                <FastImage
                  source={{
                    uri: S3Link + selectedMarker.originalPic,
                    priority: FastImage.priority.high,
                  }}
                  style={{width: width * 0.9, height: height * 0.9}}
                  resizeMode="contain"
                />
              ) : null}
            </TouchableOpacity>
          </Modal>
        </View>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
};

export default WhoLikedYouScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 0,
  },
  chipsScrollView: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 90 : 80,
    paddingHorizontal: 10,
  },
  chipsIcon: {
    marginRight: 5,
  },
  chipsItem: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 8,
    paddingHorizontal: 20,
    marginHorizontal: 10,
    height: 35,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  scrollView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 10,
    height: 300,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    padding: 10,
    elevation: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    borderRadius: 5,
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width: 2, height: -2},
    marginTop: 20,
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: 'hidden',
    flex: 1,
  },
  cardImage: {
    width: 100,
    height: 100,
    alignSelf: 'center',
    borderRadius: 8,
  },
  textContent: {
    // flex: 1,
    padding: 10,
    alignSelf: 'center',
  },
  cardtitle: {
    fontSize: 14,
    // marginTop: 5,
    fontWeight: 'bold',
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    height: 60,
  },
  marker: {
    width: 30,
    height: 30,
  },
  button: {
    alignItems: 'center',
    marginTop: 5,
    marginHorizontal: 15,
  },
  knowMore: {
    width: '100%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
    height: 25,
  },
  textSign: {
    fontSize: 14,
    fontWeight: 'bold',
  },
});
