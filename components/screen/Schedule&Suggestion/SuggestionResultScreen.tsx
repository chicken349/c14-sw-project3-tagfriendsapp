/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useCallback, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableOpacity,
  Dimensions,
  Platform,
  ScrollView,
  Modal,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useAnimatedRef} from 'react-native-reanimated';
import {mapDarkStyle, mapStandardStyle} from '../../../model/googleMapStyle';
import {useTheme} from '@react-navigation/native';
import {IRootState} from '../../../redux/store';
import {useDispatch, useSelector} from 'react-redux';
import LoadingComponent from '../Loading/LoadingComponent';
import {isAndroid} from '../../../utils/platform';
import {placeImageReq, Region} from '../../../helpers/types';
import StarRating from '../../../model/StarRating';
import FastImage from 'react-native-fast-image';
import {GoogleSuggestMarker} from '../../../redux/suggestion/state';
import {
  loadSuggestPlaces,
  updateCurrentRoomId,
} from '../../../redux/suggestion/thunk';
import {Google} from '../../../helpers/api';
import {
  wentOutFromMap,
} from '../../../redux/suggestion/action';

const {width} = Dimensions.get('window');
const CARD_HEIGHT = 240;
const CARD_WIDTH = 200;
const SPACING = 10;

const SuggestionResultScreen = ({navigation, route}: any) => {
  const theme = useTheme();
  const loadingStatus = useSelector(
    (state: IRootState) => state.suggestion.status,
  );
  const {room_id} = route?.params;
  const roomIdSaved = useSelector(
    (state: IRootState) => state.suggestion.roomId,
  );
  console.log('in room roomIdSaved', roomIdSaved);
  const thumbRef = useRef<FlatList<any> | null>(null);
  console.log('room_id', room_id);
  const markersByCategories = useSelector(
    (state: IRootState) => state.suggestion.markers,
  );
  console.log('markersByCategories', markersByCategories?.[0]?.[0].detail);
  const suggestion = useSelector(
    (state: IRootState) => state.suggestion.suggestion,
  );
  console.log('suggestion in backend', suggestion?.[0].category);

  const location = useSelector(
    (state: IRootState) => state.suggestion.location,
  );
  const mapIndex = useRef(0);
  const _map = useAnimatedRef<MapView>();
  const [modalVisible, setModalVisible] = useState(false);
  const [currentClickedIndex, setCurrentClickedIndex] = useState(0);
  const [currentRegion, setCurrentRegion] = useState<Region | undefined>();
  const [currentClickedPlaceDetail, setCurrentClickedPlaceDetail] =
    useState<GoogleSuggestMarker | undefined>();
  const [markers, setMarkers] = useState<GoogleSuggestMarker[]>();
  const [filterMode, setFilterMode] = useState(false);
  const [stateMarkers, setStateMarkers] =
    useState<GoogleSuggestMarker[] | undefined>();
  const dispatch = useDispatch();

  useEffect(() => {
    if (room_id && room_id !== '' && room_id !== roomIdSaved) {
      dispatch(updateCurrentRoomId(room_id));
      dispatch(wentOutFromMap());
      dispatch(loadSuggestPlaces(room_id));
    }
  }, [dispatch, roomIdSaved, room_id]);

  console.log('loadingStatus', loadingStatus, location, suggestion);

  useEffect(() => {
    if (markersByCategories) {
      const tempMarkerArray: GoogleSuggestMarker[] = [];
      markersByCategories.forEach(cat =>
        cat.forEach((marker, index) => {
          if (index > 4) {
            if (Math.random() < 0.3) {
              tempMarkerArray.push(marker);
            }
          } else {
            tempMarkerArray.push(marker);
          }
        }),
      );
      setMarkers(tempMarkerArray);
      console.log('markers.current to map');

      setStateMarkers(
        tempMarkerArray.filter(
          item => item.detail === tempMarkerArray[0].detail,
        ),
      );
    }
  }, [dispatch, markersByCategories]);

  const scrollToActiveIndex = useCallback(
    (index: number) => {
      console.log('scroll', index);
      console.log(
        'width',
        CARD_WIDTH,
        SPACING,
        width,
        CARD_WIDTH + SPACING - width / 2 + CARD_WIDTH / 2,
      );
      
      thumbRef?.current?.scrollToOffset({
        offset:
          index * (CARD_WIDTH + SPACING * 2) -
          width / 2 +
          CARD_WIDTH / 2 +
          SPACING * 2,
        animated: true,
      });
      const coordinate = stateMarkers
        ? stateMarkers[index].coordinate
        : {latitude: 22.2876, longitude: 114.14816};
      setCurrentClickedIndex(index);

      if (currentRegion) {
        _map?.current?.animateToRegion(
          {
            ...coordinate,
            latitudeDelta:
              currentRegion.latitudeDelta > 0.04864195044303443
                ? 0.04864195044303443
                : currentRegion.latitudeDelta,
            longitudeDelta:
              currentRegion.longitudeDelta > 0.040142817690068
                ? 0.040142817690068
                : currentRegion.longitudeDelta,
          },
          200,
        );
      }
    },
    [_map, currentRegion, stateMarkers],
  );

  useEffect(() => {
    if (filterMode) {
      scrollToActiveIndex(0);
      setFilterMode(false);
    }
  }, [filterMode, scrollToActiveIndex]);

  const onMarkerPress = useCallback(
    (index: number) => {
      console.log('google map index', index);
      mapIndex.current = index;

      scrollToActiveIndex(index);
    },
    [scrollToActiveIndex],
  );

  const onRegionChangeComplete = useCallback((mapRegion: Region) => {
    setCurrentRegion(mapRegion);
  }, []);


  if (loadingStatus === 'loading') {
    console.log('not ready!!!!!!!!!', loadingStatus);
    return <LoadingComponent navigation={navigation} />;
  }

  return (
    <View style={styles.container}>
      {markers && stateMarkers && (
        <>
          <MapView
            ref={_map}
            initialRegion={
              stateMarkers && stateMarkers.length > 0
                ? {
                    latitude: stateMarkers[0].coordinate.latitude,
                    longitude: stateMarkers[0].coordinate.longitude,
                    latitudeDelta: 0.004864195044303443,
                    longitudeDelta: 0.0040142817690068,
                  }
                : {
                    latitude: 25.2860375,
                    longitude: 114.1486955,
                    latitudeDelta: 0.004864195044303443,
                    longitudeDelta: 0.0040142817690068,
                  }
            }
            style={styles.container}
            provider={PROVIDER_GOOGLE}
            onRegionChangeComplete={onRegionChangeComplete}
            customMapStyle={theme.dark ? mapDarkStyle : mapStandardStyle}>
            {markers &&
              stateMarkers &&
              stateMarkers.map((marker, index) => {
                const scaleStyle = {
                  transform: [
                    {
                      scale: index === currentClickedIndex ? 2 : 1,
                    },
                  ],
                };

                return (
                  <Marker
                    key={index}
                    coordinate={marker.coordinate}
                    onPress={() => {
                      console.log('marker pressed', index);
                      onMarkerPress(index);
                    }}>
                    <Animated.View style={[styles.markerWrap]}>
                      <Animated.Image
                        // @ts-ignore
                        source={placeImageReq[marker.detail]}
                        // source={require('../../src/image/map_marker.png')}
                        style={[
                          styles.marker,
                          scaleStyle,
                          index === currentClickedIndex
                            ? {borderWidth: 5, borderColor: 'red'}
                            : {},
                        ]}
                        resizeMode="cover"
                      />
                    </Animated.View>
                  </Marker>
                );
              })}
          </MapView>
          <FlatList
            ref={thumbRef}
            horizontal
            data={stateMarkers}
            keyExtractor={item => item.place_id}
            contentContainerStyle={{paddingHorizontal: SPACING}}
            showsHorizontalScrollIndicator={false}
            style={styles.scrollView}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  style={[
                    styles.card,
                    index === currentClickedIndex
                      ? {
                          borderWidth: 1,
                          borderColor: 'red',
                          transform: [{translateY: -20}],
                        }
                      : {},
                  ]}
                  onPress={() => {
                    scrollToActiveIndex(index);
                  }}>
                  <View style={styles.textContent}>
                    <View style={{marginVertical: isAndroid ? 0 : 5}}>
                      <Text numberOfLines={2} style={styles.cardtitle}>
                        {item.name}
                      </Text>
                    </View>
                    <View style={{marginVertical: isAndroid ? 0 : 5}}>
                      <Text numberOfLines={1} style={styles.cardtitle}>
                        {item.detail}
                      </Text>
                    </View>
                    <View style={{marginVertical: isAndroid ? 0 : 5}}>
                      <Text numberOfLines={2} style={styles.cardtitle}>
                        {item.address}
                      </Text>
                    </View>

                    <StarRating ratings={item.rating} />
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                      }}>
                      <View style={styles.button}>
                        <TouchableOpacity
                          onPress={() => {
                            setCurrentClickedPlaceDetail({
                              coordinate: item.coordinate,
                              name: item.name,
                              rating: item.rating,
                              detail: item.detail,
                              address: item.address,
                              place_id: item.place_id,
                              photo: item.photo,
                            });
                            setModalVisible(true);
                          }}
                          style={[
                            styles.signIn,
                            {
                              borderColor: '#FF6347',
                              borderWidth: 1,
                            },
                          ]}>
                          <Text
                            style={[
                              styles.textSign,
                              {
                                color: '#FF6347',
                              },
                            ]}>
                            了解更多
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.button}>
                        <TouchableOpacity
                          onPress={() => {
                            if (suggestion) {
                              navigation.navigate('Scheduling', {
                                venue: item.name,
                                detail: item.detail,
                                address: item.address,
                                location: item.coordinate,
                                rating: item.rating,
                                place_id: item.place_id,
                                room_id: room_id,
                                searchKey: suggestion.find(
                                  sug => sug.category === item.detail,
                                )?.place,
                              });
                            }
                          }}
                          style={[
                            styles.signIn,
                            {
                              borderColor: '#FF6347',
                              borderWidth: 1,
                            },
                          ]}>
                          <Text
                            style={[
                              styles.textSign,
                              {
                                color: '#FF6347',
                              },
                            ]}>
                            在此見面
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            }}
          />
          <ScrollView
            horizontal
            scrollEventThrottle={1}
            showsHorizontalScrollIndicator={false}
            // height={50}
            style={styles.chipsScrollView}
            contentInset={{
              // iOS only
              top: 0,
              left: 0,
              bottom: 0,
              right: 20,
            }}
            contentContainerStyle={{
              paddingRight: Platform.OS === 'android' ? 20 : 0,
            }}>
            {suggestion &&
              suggestion.map((detail, index) => (
                <TouchableOpacity
                  key={index}
                  style={styles.chipsItem}
                  onPress={() => {
                    setStateMarkers(
                      markers &&
                        markers?.filter(
                          marker => marker.detail === detail.category,
                        ),
                    );
                    setFilterMode(true);
                  }}>
                  <Text>
                    {detail.category === '菜式' ? '咖啡室' : detail.category}
                  </Text>
                </TouchableOpacity>
              ))}
            <TouchableOpacity
              style={styles.chipsItem}
              onPress={() => {
                setStateMarkers(markers);
                setFilterMode(true);
              }}>
              <Text>全部</Text>
            </TouchableOpacity>
          </ScrollView>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(false);
            }}>
            <TouchableWithoutFeedback
              style={styles.centeredView}
              onPress={() => setModalVisible(false)}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 20,
                }}>
                {currentClickedPlaceDetail && (
                  <View style={styles.modalView}>
                    <View style={styles.detailContent}>
                      <View style={{width: width - 210, marginRight: 40}}>
                        <View style={{marginVertical: 5}}>
                          <Text style={styles.cardtitle}>
                            {currentClickedPlaceDetail.name}
                          </Text>
                        </View>
                        <View style={{marginVertical: 5}}>
                          <Text style={styles.cardtitle}>
                            {currentClickedPlaceDetail.detail}
                          </Text>
                        </View>
                        <View style={{marginVertical: 5}}>
                          <Text style={styles.cardtitle}>
                            {currentClickedPlaceDetail.address}
                          </Text>
                        </View>

                        <StarRating
                          ratings={currentClickedPlaceDetail.rating}
                        />
                      </View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          marginHorizontal: 15,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <FastImage
                          source={
                            currentClickedPlaceDetail.photo
                              ? {
                                  uri: `https://maps.googleapis.com/maps/api/place/photo?maxwidth=1000&maxheight=1000&photoreference=${currentClickedPlaceDetail.photo}&key=${Google}`,
                                }
                              : //@ts-ignore
                                placeImageReq[currentClickedPlaceDetail.detail]
                          }
                          // source={require('../../src/image/map_marker.png')}
                          style={styles.markerModalPic}
                          resizeMode="cover"
                        />
                      </View>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                        marginVertical: 10,
                      }}>
                      <View style={styles.button}>
                        <TouchableOpacity
                          onPress={() => {
                            setModalVisible(false);
                          }}
                          style={[
                            styles.signIn,
                            {
                              borderColor: '#FF6347',
                              borderWidth: 1,
                            },
                          ]}>
                          <Text
                            style={[
                              styles.textSign,
                              {
                                color: '#FF6347',
                              },
                            ]}>
                            返回
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.button}>
                        <TouchableOpacity
                          onPress={() => {
                            if (suggestion) {
                              navigation.navigate('Scheduling', {
                                venue: currentClickedPlaceDetail.name,
                                detail: currentClickedPlaceDetail.detail,
                                address: currentClickedPlaceDetail.address,
                                location: currentClickedPlaceDetail.coordinate,
                                rating: currentClickedPlaceDetail.rating,
                                place_id: currentClickedPlaceDetail.place_id,
                                room_id: room_id,
                                searchKey: suggestion.find(
                                  sug =>
                                    sug.category ===
                                    currentClickedPlaceDetail.detail,
                                )?.place,
                              });
                            }
                            setModalVisible(false);
                          }}
                          style={[
                            styles.signIn,
                            {
                              borderColor: '#FF6347',
                              borderWidth: 1,
                            },
                          ]}>
                          <Text
                            style={[
                              styles.textSign,
                              {
                                color: '#FF6347',
                              },
                            ]}>
                            在此見面
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                )}
              </View>
            </TouchableWithoutFeedback>
          </Modal>
          {
            <View style={styles.return}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <MaterialIcons
                  name={isAndroid ? 'arrow-back' : 'arrow-back-ios'}
                  size={isAndroid ? 30 : 25}
                  color="#333333"
                  style={{transform: [{translateX: isAndroid ? 0 : 5}]}}
                />
              </TouchableOpacity>
            </View>
          }
        </>
      )}
    </View>
  );
};

export default SuggestionResultScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 0,
  },
  chipsScrollView: {
    position: 'absolute',
    top: isAndroid ? 10 : 50,
    paddingHorizontal: 10,
    left: 40,
  },
  chipsIcon: {
    marginRight: 5,
  },
  chipsItem: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 8,
    paddingHorizontal: 20,
    marginHorizontal: 5,
    height: 35,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  scrollView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    padding: 10,
    elevation: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 5,
    marginHorizontal: 10,
    shadowColor: '#000',
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: {width: 2, height: -2},
    marginVertical: 20,
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    marginRight: SPACING,
    overflow: 'hidden',
  },
  cardImage: {
    flex: 4,
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    borderRadius: 8,
  },
  textContent: {
    flex: 1,
    padding: 10,
    // flexDirection: 'row',
    alignSelf: 'center',
  },
  cardtitle: {
    fontSize: 14,
    // marginTop: 5,
    fontWeight: 'bold',
  },
  markerWrap: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    height: 70,
    borderRadius: 1000,
    overflow: 'hidden',
  },
  marker: {
    width: 42,
    height: 42,
    borderRadius: 1000,
    overflow: 'hidden',
  },
  markerModalPic: {
    width: 100,
    height: 100,
    borderRadius: 20,
    overflow: 'hidden',
    marginRight: 20,
  },
  button: {
    alignItems: 'center',
    marginTop: 5,
  },
  signIn: {
    width: '100%',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  textSign: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  return: {
    position: 'absolute',
    top: isAndroid ? 10 : 50,
    justifyContent: 'center',
    alignItems: 'center',
    left: 10,
    backgroundColor: '#fff',
    padding: isAndroid ? 3 : 6,
    borderRadius: 50,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
  },
  modalView: {
    width: '100%',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 5,
  },
  detailContent: {
    // flex: 1,
    padding: 10,
    flexDirection: 'row',
    // alignSelf: 'center',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
