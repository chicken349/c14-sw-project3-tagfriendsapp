/* eslint-disable react-native/no-inline-styles */
import React, {useCallback, useEffect} from 'react';
import {Animated, Text, View} from 'react-native';
import {BottomTabBarHeightContext} from '@react-navigation/bottom-tabs';
import {PlatformTouchable} from '../utils/AndroidComponent';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {IRootState} from '../../redux/store';
import {loadChatroom} from '../../redux/chatroom/thunk';
import {S3Link} from '../../helpers/api';
import {formatTime} from '../utils/calculateTime';
import {ChatroomDetail, ChatroomMessage} from '../../redux/chatroom/state';
import {useState} from 'react';
import {Colors} from '../../utils/Colors';
import LoadingComponent from './Loading/LoadingComponent';
import CrushScreen from './Crush/CrushScreen';
const SPACING = 20;
const AVATAR_SIZE = 70;
const ITEM_SIZE = AVATAR_SIZE + SPACING * 3;

export default ({navigation}: any) => {
  const loadingStatus = useSelector(
    (state: IRootState) => state.chatroom.status,
  );
  const scrollY = React.useRef(new Animated.Value(0)).current;
  const userToken = useSelector((state: IRootState) => state.auth?.token);
  const socket = useSelector((state: IRootState) => state.socket.socket);
  const chatroomDetails = useSelector(
    (state: IRootState) => state.chatroom?.chatroom,
  );
  const [allChatrooms, setAllChatroom] = useState<ChatroomDetail[]>([]);
  const crushStatus = useSelector((state: IRootState) => state.crush.status);
  const crushFriendInfo = useSelector(
    (state: IRootState) => state.crush.crushHappenedFriend,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () =>
      dispatch(loadChatroom()),
    );

    return unsubscribe;
  }, [dispatch, navigation]);

  useEffect(() => {
    if (chatroomDetails) {
      setAllChatroom(chatroomDetails);
    }
  }, [chatroomDetails]);

  useEffect(() => {
    if (userToken && socket) {
      const messageUpdate = (message: ChatroomMessage) => {
        console.log('message chatroom all', message);
        setAllChatroom(prev => [
          ...prev.map(chatroom =>
            chatroom.room_id === message.roomId
              ? {
                  ...chatroom,
                  message: message.message,
                  unread: chatroom.unread
                    ? (parseInt(chatroom.unread, 10) + 1).toString()
                    : (1).toString(),
                  place_name: message.place_name
                    ? message.place_name
                    : undefined,
                }
              : chatroom,
          ),
        ]);
      };

      socket.on('message-update', messageUpdate);

      return () => {
        socket.off('message-update', messageUpdate);
      };
    }
  }, [userToken, socket]);

  const enterChat = useCallback(
    (detail: {
      name: string;
      profilePic: string;
      room_id: string;
      friend_id: number;
    }) => {
      console.log(detail);
      navigation.navigate('ChatroomDetail', {detail});
    },
    [navigation],
  );

  if (crushStatus === 'crushHappened' && crushFriendInfo) {
    return (
      <CrushScreen navigation={navigation} userPresent={crushFriendInfo} />
    );
  }

  if (loadingStatus === 'loading') {
    return <LoadingComponent />;
  }

  return (
    <BottomTabBarHeightContext.Consumer>
      {tabBarHeight => (
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.backgroundGray,
            marginBottom: tabBarHeight,
          }}>
          {chatroomDetails &&
            (chatroomDetails.length > 0 || allChatrooms.length > 0 ? (
              <Animated.FlatList
                showsVerticalScrollIndicator={false}
                data={allChatrooms}
                onScroll={Animated.event(
                  [{nativeEvent: {contentOffset: {y: scrollY}}}],
                  {useNativeDriver: true},
                )}
                keyExtractor={item => item.room_id + Math.random().toString()}
                contentContainerStyle={{
                  padding: SPACING,
                }}
                renderItem={({item, index}) => {
                  const inputRange = [
                    -1,
                    0,
                    ITEM_SIZE * index,
                    ITEM_SIZE * (index + 2),
                  ];

                  const opacityInputRange = [
                    -1,
                    0,
                    ITEM_SIZE * index,
                    ITEM_SIZE * (index + 1),
                  ];

                  const scale = scrollY.interpolate({
                    inputRange,
                    outputRange: [1, 1, 1, 0],
                  });

                  const opacity = scrollY.interpolate({
                    inputRange: opacityInputRange,
                    outputRange: [1, 1, 1, 0],
                  });
                  return (
                    <PlatformTouchable
                      onPress={() =>
                        enterChat({
                          name: item.username,
                          profilePic: S3Link + item.url,
                          room_id: item.room_id,
                          friend_id: item.true_friend_id,
                        })
                      }>
                      <Animated.View
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          padding: SPACING,
                          marginBottom: SPACING,
                          backgroundColor: 'rgba(255,255,255, 0.8)',
                          borderRadius: 12,
                          shadowColor: '#000000',
                          shadowOffset: {
                            width: 0,
                            height: 10,
                          },
                          shadowOpacity: 0.3,
                          shadowRadius: 20,
                          elevation: 50,
                          opacity,
                          transform: [{scale}],
                        }}>
                        <FastImage
                          source={{uri: S3Link + item.url}}
                          style={{
                            width: AVATAR_SIZE,
                            height: AVATAR_SIZE,
                            borderRadius: AVATAR_SIZE,
                            marginRight: SPACING / 2,
                          }}
                        />
                        <View style={{flex: 1}}>
                          <View style={{height: 26}}>
                            <Text style={{fontSize: 22, fontWeight: '700'}}>
                              {item.username}
                            </Text>
                          </View>
                          <View style={{height: 27}}>
                            <Text
                              style={{
                                fontSize: 18,
                                opacity: 0.7,
                                marginTop: 5,
                              }}>
                              {item.message
                                ? item.message
                                : item.place_name
                                ? `⚡️到${item.place_name}吧⚡️`
                                : '⚡️你們的故事已經開始⚡️'}
                            </Text>
                          </View>
                          {item.created_at && (
                            <View
                              style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                height: 19.5,
                              }}>
                              <Text
                                style={{
                                  fontSize: 12,
                                  opacity: 0.8,
                                  marginTop: 5,
                                  color: '#0099cc',
                                }}>
                                {formatTime(
                                  Date.now(),
                                  new Date(item.created_at).getTime(),
                                )}
                              </Text>
                              {item.unread && item.unread !== '0' && (
                                <View
                                  style={{
                                    backgroundColor: '#333333',
                                    height: 20,
                                    width: 20,
                                    borderRadius: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 12,
                                      opacity: 0.8,
                                      color: '#fff',
                                    }}>
                                    {item.unread}
                                  </Text>
                                </View>
                              )}
                            </View>
                          )}
                        </View>
                      </Animated.View>
                    </PlatformTouchable>
                  );
                }}
              />
            ) : (
              loadingStatus === 'loadedChatroom' && (
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'rgba(255,255,255, 0.5)',
                    position: 'absolute',
                    bottom: 0,
                    top: 0,
                    left: 0,
                    right: 0,
                  }}>
                  <Text style={{fontSize: 20, color: '#000'}}>
                    🙇🏻‍♂️暫時未有好友，快啲去💗人🙇🏻‍♀️
                  </Text>
                </View>
              )
            ))}
        </View>
      )}
    </BottomTabBarHeightContext.Consumer>
  );
};
