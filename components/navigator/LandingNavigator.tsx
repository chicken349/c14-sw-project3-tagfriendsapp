import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LandingScreen from '../screen/Landing/LandingScreen';
import SignInScreen from '../screen/Landing/SignInScreen';
import SignUpScreen from '../screen/Landing/SignUpScreen';

const LandingStack = createStackNavigator();

const LandingStackScreen = () => (
  <LandingStack.Navigator headerMode="none" mode="card">
    <LandingStack.Screen name="LandingScreen" component={LandingScreen} />
    <LandingStack.Screen
      name="SignInScreen"
      component={SignInScreen}
    />
    <LandingStack.Screen
      name="SignUpScreen"
      component={SignUpScreen}
    />
  </LandingStack.Navigator>
);

export default LandingStackScreen;
