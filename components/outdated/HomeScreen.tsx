import React, {useRef} from 'react';
import {Button, View, Text} from 'react-native';

export default function HomeScreen({navigation, route}: any) {
  const post = useRef(null);
  const [newPost, setNewPost] = React.useState(null);

  React.useEffect(() => {
    if (route.params?.post) {
      // Post updated, do something with `route.params.post`
      // For example, send the post to the server
      post.current = route.params?.post;
      setNewPost(route.params?.post);
    }
  }, [route.params?.post]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() =>
          navigation.navigate('Details', {
            // itemId: 86,
            name: 'hello',
          })
        }
      />
      <Button
        title="Create post"
        onPress={() => navigation.navigate('CreatePost')}
      />
      <Button
        title="Header Interaction"
        onPress={() => navigation.navigate('HeaderInteraction')}
      />
      <Button
        title="Update title"
        onPress={() => navigation.setOptions({title: 'Updated!'})}
      />
      <Button title="Tab" onPress={() => navigation.navigate('MyTabs')} />
      <Text style={{margin: 10}}>Post: {route.params?.post}</Text>
      <Text>{post.current}</Text>
      <Text>{newPost}</Text>
    </View>
  );
}
