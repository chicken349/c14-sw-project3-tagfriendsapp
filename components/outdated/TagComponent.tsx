import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Alert,
  // Image,
  ImageBackground,
} from 'react-native';
// import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import {BlurView} from '@react-native-community/blur';

import {Colors} from '../../utils/Colors';
import {PlatformTouchable} from '../utils/AndroidComponent';

const TagComponent = (props: {
  onSelect: () => void;
  tagClass: string;
  tags: string[];
}) => {
  const {onSelect, tagClass: userClass, tags} = props;

  return (
    <View style={styles.touchable}>
      {/* <ImageBackground
        source={require('../src/image/template.jpg')}
        style={styles.imageBackground}>
        <View style={styles.imageBackgroundContainer}> */}
      <PlatformTouchable onPress={onSelect} useForeground>
        <View>
          <ImageBackground
            source={require('../src/image/template.jpg')}
            style={styles.imageBackground}>
            <View style={styles.imageBackgroundContainer}>
              <View style={styles.tagClassContainer}>
                <Text style={styles.tagClassText}>{userClass}</Text>
              </View>

              <View style={styles.textContainer}>
                <Text style={styles.tagText}>{tags[0]}</Text>
              </View>

              <View style={styles.buttonContainer}>
                <MaterialIcons
                  name="edit"
                  size={25}
                  color={Colors.mainBlue}
                  onPress={() => {
                    Alert.alert('Edit');
                  }}
                />
              </View>
            </View>
          </ImageBackground>
        </View>
      </PlatformTouchable>
      {/* </View>
      </ImageBackground> */}
    </View>
  );
};

const styles = StyleSheet.create({
  touchable: {
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    elevation: 5,
    backgroundColor: 'white',
    height: 150,
    margin: 20,
    borderRadius: 10,
    width: Dimensions.get('window').width / 2 - 20 * 2,
    // alignSelf: 'flex-start',
  },
  imageBackground: {
    // position: 'absolute',
    width: '100%',
    height: '100%',
    // justifyContent: 'center',
    // resizeMode: 'cover',
    // overflow: 'hidden',
    // borderRadius: 10,
  },
  imageBackgroundContainer: {
    position: 'absolute',
    backgroundColor: 'rgba( 255, 255, 255, 0.7 )',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  tagClassContainer: {
    width: '100%',
    height: '37%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
    paddingTop: 10,
  },
  tagClassText: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  textContainer: {
    width: '100%',
    height: '40%',
  },
  tagText: {
    fontSize: 20,
    paddingTop: 10,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  buttonContainer: {
    width: '100%',
    height: '23%',
    // flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 10,
  },
  androidButton: {
    alignItems: 'center',
    justifyContent: 'center',
    // paddingVertical: 12,
    paddingHorizontal: 0,
    width: 70,
    height: 30,
    borderRadius: 10,
    elevation: 3,
    backgroundColor: Colors.mainBlue,
  },
  androidText: {
    fontSize: 14,
    // lineHeight: 16,
    fontWeight: '500',
    letterSpacing: 0.25,
    color: 'white',
  },
});

export default TagComponent;
