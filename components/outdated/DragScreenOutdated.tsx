import React, {useRef, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View, FlatList} from 'react-native';
import data /* , {Data} */ from './data';
import {
  Transition,
  Transitioning,
  TransitioningView,
} from 'react-native-reanimated';

const transition = (
  <Transition.Together>
    <Transition.In type="fade" durationMs={200} />
    <Transition.Change />
    <Transition.Out type="fade" durationMs={200} />
  </Transition.Together>
);

export default function DragScreen() {
  const [currentIndex, setCurrentIndex] = useState<number | null>(null);
  const ref = useRef<TransitioningView | null>(null);

  return (
    <FlatList
      data={data}
      keyExtractor={(item, index) => (item.category + index).toString()}
      renderItem={({item, index}) => (
        <Transitioning.View
          ref={ref}
          transition={transition}
          style={styles.container}>
          <TouchableOpacity
            onPress={() => {
              ref?.current?.animateNextTransition();
              setCurrentIndex(index === currentIndex ? null : index);
            }}
            style={styles.cardContainer}
            activeOpacity={0.9}>
            <View style={[styles.card, {backgroundColor: item.bg}]}>
              <Text style={[styles.heading, {color: item.color}]}>
                {item.category}
              </Text>
              {index === currentIndex && (
                <View style={styles.subCategoriesList}>
                  {item.subCategories.map((subCategory: any) => (
                    <Text
                      key={subCategory}
                      style={[styles.body, {color: item.color}]}>
                      {subCategory}
                    </Text>
                  ))}
                </View>
              )}
            </View>
          </TouchableOpacity>
        </Transitioning.View>
      )}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  cardContainer: {
    flexGrow: 1,
  },
  card: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    fontSize: 38,
    fontWeight: '900',
    textTransform: 'uppercase',
    letterSpacing: -2,
  },
  body: {
    fontSize: 20,
    lineHeight: 20 * 1.5,
    textAlign: 'center',
  },
  subCategoriesList: {
    marginTop: 20,
  },
});
