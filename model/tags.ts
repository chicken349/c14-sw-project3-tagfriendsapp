export type Tag = {
  id: number;
  category: string;
  tag: string;
};

const Tags = [
  {
    id: 1,
    category: '運動',
    tag: '排球',
  },
  {
    id: 2,
    category: '運動',
    tag: '網球',
  },
  {
    id: 3,
    category: '電影類別',
    tag: '間諜/特工片',
  },
  {
    id: 4,
    category: '電影類別',
    tag: '動畫/卡通片',
  },
  {
    id: 5,
    category: '尋求',
    tag: '認識朋友',
  },
  {
    id: 6,
    category: '宗教',
    tag: '天主教',
  },
  {
    id: 7,
    category: '年齡',
    tag: '26-30',
  },
  {
    id: 8,
    category: '星座',
    tag: '處女座',
  },
  {
    id: 9,
    category: '習慣1',
    tag: '唔吸煙',
  },
  {
    id: 10,
    category: '習慣2',
    tag: '少飲酒',
  },
  {
    id: 11,
    category: '菜式',
    tag: '台灣菜',
  },
  {
    id: 12,
    category: '玩樂',
    tag: 'VR體驗',
  },
  {
    id: 13,
    category: '文學',
    tag: '愛情文學',
  },
  {
    id: 14,
    category: '日常娛樂',
    tag: '滑板',
  },
  {
    id: 15,
    category: '學術',
    tag: '數學',
  },
  {
    id: 16,
    category: '學術',
    tag: '地理',
  },
];

// const Tags = [
//   {
//     id: 1,
//     category: 'Your pizza order placed successfully',
//     tag: 'Your pizza order to snack corner has been accepted and being processed.',
//   },
//   {
//     id: 2,
//     category: 'Your bengali thali order has been delivered',
//     tag: 'Your bengali thali has been delivered by Delicious Bong Recipe.',
//   },
//   {
//     id: 3,
//     category: 'Out for delivery',
//     tag: 'Bengali thali will reach to you within 30 minutes.',
//   },
//   {
//     id: 4,
//     category: 'Your bengali thali order placed successfully',
//     tag: 'Your bengali thali order to Delicious Bong Recipe has been accepted and being processed.',
//   },
//   {
//     id: 5,
//     category: 'Money added to your wallet',
//     tag: '₹ 1,000/- has been added to your wallet successfully and remaining balance is ₹ 1,150/-',
//   },
//   {
//     id: 6,
//     category: 'Add money to your wallet',
//     tag: 'Only ₹ 150/- is left in your wallet. Add some more amount to place your order quickly.',
//   },
//   {
//     id: 7,
//     category: 'Check new Pizza Corner within 1 km',
//     tag: 'A new Pizza Corner is being loved by more people around you.',
//   },
//   {
//     id: 8,
//     category: 'Check new Roll Center within 3 km',
//     tag: 'A new roll center is being loved by more people around you.',
//   },
//   {
//     id: 9,
//     category: 'Check new Crispy Chicken within 3 km',
//     tag: 'A new Crispy Chicken is being loved by more people around you.',
//   },
//   {
//     id: 10,
//     category: 'Check new Snacks Corner within 5 km',
//     tag: 'A new Snacks Corner is being loved by more people around you.',
//   },
// ];

export default Tags;
