export const Colors = {
  mainBlue: 'rgb(57,134,176)',
  backgroundGray: '#efefef',
  loginGreen: '#009387',
  starOrange: '#FF8C00',
};
